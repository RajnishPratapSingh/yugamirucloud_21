﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace CloudManager
{
    public static class CloudSystem
    {
        public static class AppDB
        {
            public static CloudManager.Cloud.PatientRecord Max = CloudUtility.MaxLatest();
        }
        public static class CloudDB
        {
           public static CloudManager.Cloud.PatientRecord Max = CloudUtility.MaxLatest("cpdb.sqlite");
        }
        public static List<string> Updater(List<string> lstUniqueIds)
        {
            List<string> lstUpdateDoneIDs = new List<string>();
            //For one update we need to send four request (each request for one table)
            foreach(string uid in lstUniqueIds)
            {
                //get local db record from four table
                DataTable dtPatientdetails = AppDataReader.GetPatientDetails(uid);
                DataTable dtKneedown = AppDataReader.GetFrontBodyPositionKneeDown(uid);
                DataTable dtStanding = AppDataReader.GetFrontBodyPositionStanding(uid);
                DataTable dtSide = AppDataReader.GetSideBodyPosition(uid);

                //Get JSON string for each table. tables will have one record at a time 
                ConvertTableToJSON converterPatientDetails = new ConvertTableToJSON(dtPatientdetails, Tables.PatientDetails);
                //we first need to send this json string to server then will proceed for next table json, 
                //at a time one json will be in memory
                string json = converterPatientDetails.GetJson();
                //Upload above JSON to server
                
                //Check response from server

                //Updtae local database for lut field.
                


                //string strJsonPdetails=JSONManager.getJSONToSend(Tables.PatientDetails)
            }



            return lstUpdateDoneIDs;
        }
    }
}
