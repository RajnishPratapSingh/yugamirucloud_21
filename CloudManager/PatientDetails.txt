[
    {
        "TableName": "PatientDetails",
        "ComputerId": "ABCDEFGHIJKLMNOP123654QRST",
        "ActivationKey": " ABCDEFGHIJKLMNOP123654QRST ",
         "UniqueId": "123",
        "BenchmarkDistance": "80",
        "Comment": "",
        "DOB": "01-Jan-80 12:00:00 AM",
        "Date": "1",
        "Gender": "1",
        "Height": "100",
        "MeasurementTime": "18-04-08 01:35",
        "Month": "1",
        "Name": "SumitA",
        "PatientId": "S1",
        "Year": "1980",
        "lut": "8-4-2018-1-39-36"
    }
]
