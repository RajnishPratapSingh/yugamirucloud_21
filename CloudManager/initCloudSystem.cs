﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public static class initCloudSystem
    {

        /// <summary>
        /// This function will create the sqlite database for cloud System from Existing yugamiru.sqlite database of Desktop Application.
        /// Do nothing if File exist
        /// </summary>
        /// <param name="AppDBAddress">Local path of the yugamiru.sqlite database file</param>
        /// <returns></returns>
        public static bool CreateCloudDatabase(string AppDBAddress= @"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite")
        {
            if(File.Exists(@"cpdb.sqlite"))
            {
                //No Action
            }
            else
            {
                //copy the existing AppDB to local cloud system.
                File.Copy(AppDBAddress, @"cpdb.sqlite");
            }
            return true;
        }
        /// <summary>
        /// This function will create the sqlite database for cloud System from Existing yugamiru.sqlite database of Desktop Application.
        /// Do nothing if File exist
        /// </summary>
        /// <param name="AppDBAddress">Local path of the yugamiru.sqlite database file</param>
        /// <returns></returns>
        public static bool CreateAppDatabase(string AppDBAddress = @"cpdb.sqlite")
        {
            if (File.Exists(@"cpdb.sqlite"))
            {
                //No Action
            }
            else
            {
                //copy the existing AppDB to local cloud system.
                File.Copy(AppDBAddress, @"cpdb.sqlite");
            }
            return true;
        }

    }

}
