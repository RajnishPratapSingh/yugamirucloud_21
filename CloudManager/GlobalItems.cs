﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public class GlobalItems
    {
        static string _ComputerID = string.Empty;
        static string _ActivationKey = string.Empty;
        static string _sqliteDBPath = @"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite"; //@"E:\GSPORT\SQLDBTest\cpdb.sqlite";
        public static bool SetValues(string computerID,string activationKey)
        {
            //_ActivationKey = "4444kgjyftyfjvjhg"; it is for testing
            //_ComputerID= "55555hgjyfjhg";         it is for testing

            if (_ActivationKey.Length == 0 && _ComputerID.Length == 0)
            {
                _ActivationKey = activationKey;
                _ComputerID = computerID;
            }
            return true;
        }
        public static string ActivationKey
        {
            get
            {
                if(_ActivationKey.Length==0)
                {
                    throw new Exception("Activation key has not been initialize yet");
                }
                return _ActivationKey;
            }
        }
        public static string ComputerID
        {
            get
            {
                if (_ComputerID.Length == 0)
                {
                    throw new Exception("ComputerID has not been initialize yet");
                }
                return _ComputerID;
            }
        }
        public static string sqliteDBPath
        {
            get
            {
                return _sqliteDBPath;
            }
        }
        static string _StallID = string.Empty;
        public static string StallID
        {
            get
            {
                if (_StallID.Length == 0)
                {
                    try
                    {
                        SetInstallIDs();
                    }
                    catch
                    {
                        throw new Exception("Install ID has not been initialize yet");
                    }
                }
                return _StallID;
            }
        }
        static string _reg_User_ID = string.Empty;
        public static string Reg_User_ID
        {
            get
            {
                if (_reg_User_ID.Length == 0)
                {
                    try
                    {
                        SetInstallIDs();
                    }
                    catch
                    {
                        throw new Exception("Reg_User_ID has not been initialize yet");
                    }
                }
                return _reg_User_ID;
            }
        }

        private static void SetInstallIDs()
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + @"C:\ProgramData\gsport\Yugamiru cloud\database\Yugamiru.sqlite");// + Constants.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Stall_ID, Reg_User_ID from tblInstallInfo where sync='THIS'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                if (dt.Rows.Count == 1)
                {
                    _StallID = dt.Rows[0]["Stall_ID"].ToString();
                    _reg_User_ID = dt.Rows[0]["Reg_User_ID"].ToString();
                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
