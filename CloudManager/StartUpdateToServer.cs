﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    public static class StartUpdateToServer
    {
        public static void StartSending()
        {
            //get updated record's ID from local database
            List<string> lstids = AppDataReader.GetUniqueIDsNeedToUpdate();

            if (lstids.Count == 0)
                return;

            //do for each id
            foreach(string id in lstids)
            {
                //get updated row of this id from 4 tables
                DataTable dtPatient = AppDataReader.GetPatientDetails(id);
               



                //get json and send to server one by one (to reduce memory consumption)
                //upload patient table
                ConvertTableToJSON convertPatient = new ConvertTableToJSON(dtPatient, Tables.PatientDetails, RowNumToJSON.FirstRow);
                bool uploadPatientResult = JSONManager.JsonTOBsonSender(convertPatient.strJonToSend, CloudUrls.PatientDetailsSendToUrl);
                if(uploadPatientResult)
                {
                    DataTable dtKneeDown = AppDataReader.GetFrontBodyPositionKneeDown(id);
                    //upload kneedown table.

                    //Sumit V2.0 on 08-May-18 
                    //add some string values to imagesbytes data (at data start and at data end) 
                    dtKneeDown = DBHandler.AddToImageBytesBeforeSend(dtKneeDown);






                    ConvertTableToJSON convertkneedown = new ConvertTableToJSON(dtKneeDown, Tables.FrontBodyPositionKneeDown, RowNumToJSON.FirstRow);
                    bool uploadkneedownResult = JSONManager.JsonTOBsonSender(convertkneedown.strJonToSend, CloudUrls.FrontBodyPositionKneeDownSendToUrl);





                    //upload standing table
                    DataTable dtStanding = AppDataReader.GetFrontBodyPositionStanding(id);

                    //Sumit v2.0
                    dtStanding = DBHandler.AddToImageBytesBeforeSend(dtStanding);
                    //v2.0

                    ConvertTableToJSON convertStanding = new ConvertTableToJSON(dtStanding, Tables.FrontBodyPositionStanding, RowNumToJSON.FirstRow);
                    bool uploadStandingResult = JSONManager.JsonTOBsonSender(convertStanding.strJonToSend, CloudUrls.FrontBodyPositionStandingSendToUrl);



                    DataTable dtSideBody = AppDataReader.GetSideBodyPosition(id);

                    //sumit v2.0
                    dtSideBody = DBHandler.AddToImageBytesBeforeSend(dtSideBody);
                    //v2.0


                    //upload side position table
                    ConvertTableToJSON convertSidebody = new ConvertTableToJSON(dtSideBody, Tables.SideBodyPosition, RowNumToJSON.FirstRow);
                    bool uploadSidebodyResult = JSONManager.JsonTOBsonSender(convertSidebody.strJonToSend, CloudUrls.SideBodyPositionSendToUrl);

                    //Mark relevant record, update done
                    DBHandler.ResetLut(id);

                }
                //string patientUploadResult=
            }


        }
    }
}
