﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SQLite;
using System.Text;
using System.Net;
using System.IO;

using Newtonsoft.Json;

namespace CloudManager
{
   public static class InstallInfoManager
    {
        //#region
        public static bool SyncInstallInfo()
        {
            bool syncSuccess = false;
            if (!WebComCation.Utility.IsInternetConnected())
            {
                return false;
            }
            string req = "[{\"Computer_id\": \""+CloudManager.GlobalItems.ComputerID+"\",\"Activation_key\": \""+CloudManager.GlobalItems.ActivationKey+"\",\"Install_id\":\""+GlobalItems.StallID+"\",\"reg_user_id\":\""+GlobalItems.Reg_User_ID+"\"}]";
            //string req = "[{\"Computer_id\": \"BFEBFBFF000006FDC62DF8AB4C4C454400315010805AC8C04F434258\",\"Activation_key\": \"BRGQ080W0031C13C888V3X1DB45JQXQ\",\"Install_id\":\"9\",\"reg_user_id\":\"855\"}]";
            //string req = "[{\"Computer_id\": \"BFEBFBFF000406E378F65ECF4C4C4544005A4B108044C6C04F584632\",\"Activation_key\": \"BYGN0Y0Z0031513C8585371SEK5P5FF\",\"Install_id\":\"5\",\"reg_user_id\":\"852\"}]";

            String queryString = req;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            JsonSerializer jsonSerializer = new JsonSerializer();
            MemoryStream objBsonMemoryStream = new MemoryStream();
            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            jsonSerializer.Serialize(bsonWriterObject, studentObject);
            byte[] requestByte = objBsonMemoryStream.ToArray();
            //Connect to our Yugamiru Web Server
            //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
            WebRequest webRequest = WebRequest.Create("http://52.197.210.82/apioauthdata/index.php/home/dwnldinstallationdetails");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {

            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            webDataStream = webResponse.GetResponseStream();

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();
            if (responseFromServer.ToUpper().Contains("Data Doesnot exist".ToUpper()))
                return true;
            ProcessBsonInstallData(responseFromServer);
            syncSuccess = true;

            //=========

            //=======



            //GetRemoverStringsTable();
            return syncSuccess;
        }
        public static  DataTable GetRemoverStringsTable()
        {
            DataTable dtStringsToReplace = new DataTable();
            dtStringsToReplace.Columns.Add("StringToReplace", typeof(String));
            //Add unwanted or junk strings coming from server in BSON data
            //string st = "\0\u0004\0\0\0Sumit";
            dtStringsToReplace.Rows.Add("\0\u0001\0\0\0\0\0\u00031\0]\u0001\0\0\u0002");
            dtStringsToReplace.Rows.Add("\0\u0004\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u0002\0\0\0");//\0\u0003\0\0\0
            dtStringsToReplace.Rows.Add("\0\u0003\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u0005\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u0006\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u0007\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u0008\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u0009\0\0\0");//
            dtStringsToReplace.Rows.Add("\0\u001d\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u000e\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u0002");
            dtStringsToReplace.Rows.Add("\09\0\0\0");
            dtStringsToReplace.Rows.Add("\0\n\0\0\0");
            dtStringsToReplace.Rows.Add("\0\v\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u0006\0\0\0");
            dtStringsToReplace.Rows.Add("\0\r\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u0001");
            dtStringsToReplace.Rows.Add("\0\u0011\0\0\0");//\0\u0011\0\0\0
            dtStringsToReplace.Rows.Add("\0\u0015\0\0\0");//\0\a\0\0\0
            dtStringsToReplace.Rows.Add("\0\u000e\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u000f\0\0\0");

            dtStringsToReplace.Rows.Add("\0\u0010\0\0\0");
            dtStringsToReplace.Rows.Add("\0%\0\0\0");
            dtStringsToReplace.Rows.Add("\0\u0005\0\0\0");
            dtStringsToReplace.Rows.Add("\0\a\0\0\0");


            dtStringsToReplace.Rows.Add("\0\b\0\0\0");
            dtStringsToReplace.Rows.Add("\0\f\0\0\0");
            dtStringsToReplace.Rows.Add("\0\t\0\0\0");
            dtStringsToReplace.Rows.Add("\0!\0\0\0");
            dtStringsToReplace.Rows.Add("\0)\0\0\0");
            dtStringsToReplace.Rows.Add("\0\0\0\0");
            //dtStringsToReplace.Rows.Add(Environment.NewLine);
            dtStringsToReplace.Rows.Add("\0");
            return dtStringsToReplace;




            //MessageBox.Show(dtStringsToReplace.Rows[0][0].ToString());
            //st=st.Replace(dtStringsToReplace.Rows[0][0].ToString(),"");
            //
            //return new DataTable();
        }
        public static string GetBase64ToNormal(string bs64Str)
        {
            byte[] bts = Convert.FromBase64String(bs64Str);
            Encoding enc = Encoding.GetEncoding("ISO-8859-6");
            string normal = enc.GetString(bts);
            return normal;
        }

        /// <summary>
        /// Processes bson data and saves it in local DB
        /// </summary>
        /// <param name="bsonDataWhole"></param>
        /// <returns></returns>
        public static DataTable ProcessBsonInstallData(string bsonDataWhole)
        {

            //string InnoStr = Regex.Replace(bsonDataWhole, @"[^\w\.@+/=-]", " ");



            DataTable dt = GetRemoverStringsTable();
            string[] strFrstSplitOn = { "Status" };
            #region Imp Comments
            /* string[] strSplitOnFields = {   "Status",
                                        "Computer_id",
                                        "Activation_key",
                                        "stall_id",
                                        "reg_user_id",
                                        "Computer_name",
                                        "Installation_name",
                                        "Date_of_install",
                                        "Installed_by",
                                        "Comment",
                                        "Language" };*/
            #endregion

            string[] strSplitOnFields = {
                                        "stall_id",
                                        "reg_user_id",
                                        "Computer_name",
                                        "Installation_name",
                                        "Date_of_install",
                                        "Installed_by",
                                        "Comment",
                                        "Language" };

            DataTable dtDnPCFamilyInfo = new DataTable();
            dtDnPCFamilyInfo.Columns.Add("stall_id", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("reg_user_id", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("Computer_name", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("Installation_name", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("Date_of_install", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("Installed_by", typeof(String));
            dtDnPCFamilyInfo.Columns.Add("Comment", typeof(String));
            //need not to store language.

            List<string> lstPCs = new List<string>(bsonDataWhole.Split(strFrstSplitOn, StringSplitOptions.None));

            foreach (string strPcInstallInfo in lstPCs)
            {
                List<string> vals = new List<string>(strPcInstallInfo.Split(strSplitOnFields, StringSplitOptions.None));
                if (vals.Count > 1)
                {
                    vals.RemoveAt(0);
                    if (vals.Count > 1)
                    {
                        vals.RemoveAt(vals.Count - 1);
                    }
                }
                else
                {
                    continue;
                }

                for (int i = 0; i < vals.Count; i++)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        vals[i] = vals[i].Replace(dr[0].ToString(), "");
                    }
                }
                dtDnPCFamilyInfo.Rows.Add(vals.ToArray());
            }


            //


            //Convert comments from Base64 to normal string
            for(int i=0;i< dtDnPCFamilyInfo.Rows.Count;i++)
            {
                //string s = dtDnPCFamilyInfo.Rows[i]["comment"].ToString();
                dtDnPCFamilyInfo.Rows[i]["comment"] = Regex.Replace(dtDnPCFamilyInfo.Rows[i]["comment"].ToString(), @"[^\w\.@+/=-]", "");

                dtDnPCFamilyInfo.Rows[i]["comment"] = GetBase64ToNormal(dtDnPCFamilyInfo.Rows[i]["comment"].ToString());
            }

            SaveInstallInfoTable(dtDnPCFamilyInfo);

            return dtDnPCFamilyInfo;
        }
       public static void SaveInstallInfoTable(DataTable dtInsInfo)
        {
            //Update database
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection("Data Source=" + @"C:\ProgramData\gsport\Yugamiru cloud\database\Yugamiru.sqlite");// + Constants.db_file);
            SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            cmd = sqlite.CreateCommand();
            try
            {
                foreach (DataRow dr in dtInsInfo.Rows)
                {
                    //first check if stall_id + reg_user_id combination exists then update else insert
                    {
                        cmd.CommandText = "select id from tblInstallInfo where stall_id='" + dr["stall_id"] + "' AND reg_user_id='" + dr["reg_user_id"] + "';";
                        DataTable dt = new DataTable();
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        da.Fill(dt);
                        if (dt.Rows != null && dt.Rows.Count == 1)
                        {
                            //update existig
                            cmd.CommandText = "	update tblInstallInfo set " +
                                                    " Computer_name = '" + dr["Computer_name"] + "'," +
                                                    " Installation_name = '" + dr["Installation_name"] + "'," +
                                                    " Date_of_install = '" + dr["Date_of_install"] + "'," +
                                                    " Installed_by = '" + dr["Installed_by"] + "'," +
                                                    " Comment = '" + dr["Comment"] + "' " +
                                                    "where stall_id = '" + dr["stall_id"] + "' AND reg_user_id = '" + dr["reg_user_id"] + "'; ";

                            cmd.ExecuteNonQuery();
                        }
                        else if (dt.Rows == null || dt.Rows.Count == 0)
                        {
                            //insert new 
                            cmd.CommandText = "INSERT into tblInstallInfo(stall_id,	reg_user_id,	Computer_name,	Installation_name,	Date_of_install,	Installed_by,	Comment)" +
                                                                      "values('" + dr["stall_id"] + "','" + dr["reg_user_id"] + "','" + dr["computer_name"] + "'," +
                                                                       "'" + dr["Installation_name"] + "', '" + dr["Date_of_install"] + "'," +
                                                                       "'" + dr["Installed_by"] + "',  '" + dr["Comment"] + "'); ";
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            throw new Exception("Multiple records found for one installation");
                        }

                    }
                }
                sqlite.Close();
            }
            catch (SQLiteException qex)
            {
                //MessageBox.Show("DB Error at InsInfo Sync");
            }
        }
    }
}
