﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;



namespace CloudManager
{
    public static class JSONManager
    {
        public static string getJSONToSend(Tables tableName = Tables.PatientDetails)
        {
            string jsonContent = (new ConvertTableToJSON((new AppDataReader()).ReadTableData(tableName), tableName)).GetJSONPatientDetails();

            return jsonContent;
        }

        public static bool JsonTOBsonSender(string json, string url)
        {
            //============Testing for JSON direct save to file
            //string filename = "";
            //if(json.Contains("KneeDown"))
            //{
            //    filename = "kneecoding.txt";
            //}
            //else if (json.Contains("Standing"))
            //{
            //    filename = "standingcoding.txt";
            //}
            //else if (json.Contains("SideBody"))
            //{
            //    filename = "siedbodycoding.txt";
            //}
            //if(filename!="")
            //{
            //    File.WriteAllText(filename, json);
            //}
            //===============================================

            try
            {
                String queryString = json;

                //queryString = queryString.Replace("\\", @"\\");//Added by Rajnish For GSP-901//
                //queryString = queryString.Replace("\\", @"\\").Replace("\"\"", "\\\"");//Added by Rajnish For GSP-901//   --commented by sumit due to GSP-944             

                #region Sample Query JSON

                //==================================================
                //queryString = "[{\"Computer_id\": \"BFEBFBFF000406E4\",\"Computer_name\": \"DESKTOP-9QLKVGO\",\"Activation_key\": \"BWHP0T0A00AG712J8Q8Z391HUG8KTH6\"}]";

                //=====================================================
                #endregion

                //commented by Rajnish For GSP-901//
                //var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(JsonConvert.ToString(queryString));// queryString;//Newtonsoft.Json.JsonConvert.DeserializeObject(@"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]");//queryString);
                //3.
                //Added by Rajnish For GSP-901//
                //JsonSerializerSettings settings = new JsonSerializerSettings
                //{
                //    StringEscapeHandling = StringEscapeHandling.EscapeHtml
                //};

                //var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString, settings);--commented by sumit GSP-944
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //----------------------------//
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);

                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);

                #region sample Json
                //data = @"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]";

                //================================================================================
                #endregion


                byte[] requestByte = objBsonMemoryStream.ToArray();
                WebRequest webRequest = WebRequest.Create(url);     //(@"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data");//CloudUrls.FrontBodyPositionKneeDownSendToUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;

                // create our stram to send
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                //webResponse.ContentType = "text/html; charset=ASCII";
                string tp = webResponse.ContentType;
                try
                {
                    webDataStream = webResponse.GetResponseStream();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();

                //=========================================================
                //byte[] data = Convert.FromBase64String("MQAAAAJOYW1lAA8AAABNb3ZpZSBQcmVtaWVyZQAJU3RhcnREYXRlAMDgKWE8AQAAAA==");

                //MemoryStream ms = new MemoryStream(data);
                //using (BsonReader reader = new BsonReader(ms))
                //{
                //    JsonSerializer serializer = new JsonSerializer();

                //    //Event e = serializer.Deserialize<Event>(reader);
                //    var e = serializer.Deserialize(reader);

                //    //Console.WriteLine(e.Name);
                //    // Movie Premiere
                //}
                //=========================================================
            }
            catch (Exception ex)
            { }


            #region commented
            //Encoding ad = webResponseSReader.CurrentEncoding;
            //string v1 = responseFromServer.Replace(@"\u", "=");
            ////byte[] arb = Convert.ToByte(responseFromServer);
            ////string sd = Convert.ToBase64String( 
            //string input = responseFromServer;
            //var utf8bytes = Encoding.UTF8.GetBytes(input);
            //var win1252Bytes = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, utf8bytes);
            //string s_unicode2 = System.Text.Encoding.UTF8.GetString(win1252Bytes);
            //Encoding utf8 = Encoding.UTF8;
            //Encoding ascii = Encoding.ASCII;            
            //string output = ascii.GetString(Encoding.Convert(utf8, ascii, utf8.GetBytes(input)));


            ////var a1[] = ASCIIEncoding.

            //string val2 = responseFromServer.Replace("\\u", "*");
            //string val3 = val2.Replace(@"\u", ":");
            //string charat = @"\u";
            //string AnsRes = (new UriTypeConverter()).ConvertToInvariantString(responseFromServer);
            //string AnsRes1 = (new UriTypeConverter()).ConvertToString(responseFromServer);
            //AnsRes1 = AnsRes1.Replace("\0", "").Replace("�", "-");//
            #endregion
            return true;
        }
    }
}
