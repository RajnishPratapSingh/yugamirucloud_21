﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Data.SQLite;

namespace LicenseInformation
{
    public partial class frmCloudSycnSettings : Form
    {
        SQLiteConnection sqlite;
        public frmCloudSycnSettings()
        {
            InitializeComponent();
        }

        private void frmCloudSycnSettings_Load(object sender, EventArgs e)
        {
            btnRefresh_Click(null, null);
        }
        public DataTable GetInstallInfoTable()
        {
            //Get Refresh data from Online
            if (!WebComCation.Utility.IsInternetConnected())
            {
                MessageBox.Show("Working with old data" + Environment.NewLine + "No Internet.Cannot sync with latest data.");
            }
            else
            {
                CloudManager.InstallInfoManager.SyncInstallInfo();                
            }
            //

            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {                
                sqlite = new SQLiteConnection("Data Source=" + @"C:\ProgramData\gsport\Yugamiru cloud\database\Yugamiru.sqlite");// + Constants.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select * from tblInstallInfo";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                sqlite.Close();
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
            //sqlite.Close();


            return dt;
        }

       
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            DataTable dt = GetInstallInfoTable();
            flpnlPCs.Controls.Clear();
            flpnlPCs.Tag = null;
            flpnlPCs.Tag = dt;

            

            foreach(DataRow dr in dt.Rows)
            {
                CustomControls.PC pC = new CustomControls.PC();
                //Extract Values
                string id= dr["id"].ToString();
                string stall_id = dr["stall_id"].ToString();
                string reg_user_id = dr["reg_user_id"].ToString();
                string computer_Name = dr["computer_Name"].ToString();
                string installation_name = dr["installation_name"].ToString();
                string date_of_install = dr["date_of_install"].ToString();
                string installed_by = dr["installed_by"].ToString();
                string comment = dr["comment"].ToString();
                string language = dr["language"].ToString();                
                string Sync = dr["sync"].ToString();

                //Set PC info
                pC.SetCommentTip(comment);
                pC.txtPCName.Text = computer_Name;
                pC.txtInsName.Text = installation_name;
                pC.txtInstallDate.Text = date_of_install;

                pC.strComments = comment;
                pC.strComputerName = computer_Name;
                pC.strDateOfInstall = date_of_install;
                pC.strInstallationName = installation_name;
                pC.strInstalledBy = installed_by;
                pC.strLanguage = language;
                pC.strRegUserId = reg_user_id;
                pC.strRemarks = comment;
                pC.strStallId = stall_id;



                if (Sync.ToUpper()=="TRUE")
                {
                    pC.chkSelected.Checked = true;
                }
                else if (Sync.ToUpper() == "FALSE")
                {
                    pC.chkSelected.Checked = false;
                }
                else if (Sync.ToUpper() == "THIS")
                {
                    pC.chkSelected.Checked = true;
                    pC.chkSelected.ThreeState = true;
                    pC.Enabled = false;
                }              

                this.flpnlPCs.Controls.Add(pC);
                this.flpnlPCs.Controls[flpnlPCs.Controls.Count - 1].Focus();
            }
        }

        private void btnSaveSync_Click(object sender, EventArgs e)
        {
            DataTable dt;
            if (flpnlPCs.Tag != null)
            {
                dt = ((DataTable)(flpnlPCs.Tag));
                foreach(Control ct in flpnlPCs.Controls)
                {
                    CustomControls.PC pc = ((CustomControls.PC)(ct));
                    if (!pc.Enabled)
                        continue;

                    for(int k=0;k<dt.Rows.Count;k++)
                    {
                        if(pc.strStallId==dt.Rows[k]["stall_id"].ToString())
                        {
                            dt.Rows[k]["sync"] = pc.chkSelected.Checked ? "TRUE" : "FALSE";
                        }
                    }
                }
                //Update database
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                string qry = "";
                foreach(DataRow dr in dt.Rows)
                {
                    qry += "update tblInstallInfo set sync='" + dr["sync"] + "' where stall_id='" + dr["stall_id"] + "';";
                }
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
                MessageBox.Show("Settings saved successfully.");                
            }
            else
                return;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
