﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WebComCation
{
    public static class ProductInfo
    {
        public static int ProductID { get; set; }
        public static string ProductName { get; set; }
        public static int ProductVersionMajor { get; set; }
        public static int ProductVersionMinor { get; set; }
        public static string ProductPersistencyKey { get; set; }
        public static string ProductEncryptionKey { get; set; }
        static ProductInfo()
        {
            ProductID = 3;
            ProductName = "gsport";
            ProductVersionMajor = 1;
            ProductVersionMinor = 0;
            ProductPersistencyKey = "{1d683c4c-a831-40f0-8301-726f010f5ee9}";
            ProductEncryptionKey = "DmpuTTAjm3gV4A==";
        }

    }
}
