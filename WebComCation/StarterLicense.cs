﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
//using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;

namespace WebComCation
{
    public class StarterLicense
    {
        public static string activateURL;
        public static string validateURL;
        public static string releaseURL;


        static StarterLicense()
        {
            activateURL = ConfigurationManager.AppSettings["Activate"];
            validateURL = ConfigurationManager.AppSettings["Validate"];
            releaseURL = ConfigurationManager.AppSettings["Release"];
        }
    }
}
