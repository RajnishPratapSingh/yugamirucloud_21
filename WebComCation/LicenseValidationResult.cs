﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WebComCation
{
    /// <summary>
    /// this class holds the result information for the Validation test.
    /// </summary>
    public class LicenseValidationResult
    {
        public LicenseStatus status { get; set; }
        public string Message { get; set; }

    }
}
