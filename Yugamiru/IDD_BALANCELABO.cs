﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Yugamiru
{
    public partial class IDD_BALANCELABO : Form
    {
        /// Tested By Suhana For bit bucket
        JointEditDoc m_JointEditDoc;
        public int screen_width = 0;
        PictureBox picturebox1 = new PictureBox();

        public IDD_BALANCELABO(JointEditDoc GetDocument)
        {
            InitializeComponent();

            Bitmap bmBack1 = Yugamiru.Properties.Resources.Mainpic;
            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.BackColor = Color.Transparent;
            this.Controls.Add(picturebox1);
            picturebox1.Image = bmBack1;


            GetDocument.m_FrontBodyPositionKneedown = null;
            GetDocument.m_FrontBodyPositionStanding = null;
            GetDocument.m_SideImageBytes = null;

            GetDocument.m_FrontKneedownImageBytes = null;
            GetDocument.m_FrontStandingImageBytes = null;

           
            

            if (IDC_MeasurementBtn.Image != null)
                IDC_MeasurementBtn.Image.Dispose();
            else
            {
              /*  if (GetDocument.GetInputMode() == Constants.INPUTMODE_NONE)
                    IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_up;
                else*/
                    IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_on;
            }
            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_up;
            IDC_CloseBtn.Image = Yugamiru.Properties.Resources.end_up;
            IDC_SETTING_BTN.Image = Yugamiru.Properties.Resources.setting_up;
            m_JointEditDoc = GetDocument;

        }


        private void IDD_BALANCELABO_Load(object sender, EventArgs e)
        {

        }

        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        static string m_CLICK_MODE = "NONE";
        private void IDC_MeasurementBtn_Click(object sender, EventArgs e)
        {
            IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_Tick);
            t.Start();
            m_CLICK_MODE = "START";
        }
        void timer_Tick(object sender, EventArgs e)
        {
            switch (m_CLICK_MODE)
            {
                case "START":
                    m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
                    m_JointEditDoc.ClearMakerTouchFlag();
                    m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
                    m_JointEditDoc.InitBodyBalance();
                    m_JointEditDoc.ClearStandingImage();
                    m_JointEditDoc.ClearKneedownImage();
                    m_JointEditDoc.ClearSideImage();
                    m_JointEditDoc.SetDataMeasurementTime("");
                    m_JointEditDoc.SetSaveFilePath("");
                    m_JointEditDoc.ChangeToMeasurementView();

                    //CloseForm(EventArgs.Empty); // triggerring the event, to send it to form1 which is base form - step3
                    this.Visible = false;
                    m_JointEditDoc.GetMeasurementDlg().Visible = true;
                    m_JointEditDoc.GetMeasurementDlg().RefreshForm();
                    m_JointEditDoc.GetMeasurementDlg().Reload();
                    
                    t.Stop();
                    //this.Close();
                    //DisposeControls();
                    break;
                case "OPENRESULT":
                    this.Visible = false;
                    m_JointEditDoc.GetOpenResult().Visible = true;
                    m_JointEditDoc.GetOpenResult().RefreshForms();
                    t.Stop();
                    break;
                case "SETTING":
                    m_JointEditDoc.SetSettingMode(Constants.SETTING_SCREEN_MODE_TRUE);
                    this.Visible = false;
                    m_JointEditDoc.GetSettingView().RefreshForms();
                    m_JointEditDoc.GetSettingView().Visible = true;

                    t.Stop();
                    break;
                case "CLOSE":
                    this.Close();
                    DisposeControls();
                    Application.Exit();
                    t.Stop();
                    break;
            }
        }


        public void IDD_BALANCELABO_SizeChanged(object sender, EventArgs e)
        {
            // Set the border style to a three-dimensional border.
            //this.picturebox1.BorderStyle = BorderStyle.Fixed3D;
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = (this.ClientSize.Height - picturebox1.Height ) / 2;
            this.panel1.Left = (this.Width - panel1.Width) / 2;
            this.panel1.Top = (this.ClientSize.Height - panel1.ClientSize.Height)/2 + 230;
        }
        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_CloseBtn_Click(object sender, EventArgs e)
        {
            IDC_CloseBtn.Image = Yugamiru.Properties.Resources.end_on;
            t.Interval = 100;
            t.Start();
            t.Tick += new EventHandler(timer_Tick);

            m_CLICK_MODE = "CLOSE";

        }


        private void IDC_SETTING_BTN_Click(object sender, EventArgs e)
        {
            IDC_SETTING_BTN.Image = Yugamiru.Properties.Resources.setting_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_Tick);
            t.Start();
            m_CLICK_MODE = "SETTING";
        }
        public event EventHandler OpenSettingScreen; // creating event handler - step1
        public void FunctionToOpenSettingScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = OpenSettingScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public void DisposeControls()
        {
            IDC_MeasurementBtn.Image.Dispose();
            IDC_AnalysisBtn.Image.Dispose();
            IDC_CloseBtn.Image.Dispose();
            IDC_SETTING_BTN.Image.Dispose();

            Yugamiru.Properties.Resources.startgreen_on.Dispose();
            Yugamiru.Properties.Resources.dataload_up.Dispose();
            Yugamiru.Properties.Resources.end_up.Dispose();
            Yugamiru.Properties.Resources.setting_up.Dispose();
            Yugamiru.Properties.Resources.Mainpic.Dispose();


            this.Dispose();
            this.Close();
        }

        private void IDD_BALANCELABO_Paint(object sender, PaintEventArgs e)
        {
            if (m_JointEditDoc.GetQRCodeFlag())
            {               
                    e.Graphics.DrawImage(m_JointEditDoc.GetQRCodeImage(), (this.Width - 600) / 2, /*(this.Height - 600)/2,*/0, 600, 600);                
            }
            else
            {
            /*
                Bitmap bmp;
                using (bmp = Yugamiru.Properties.Resources.Mainpic)
                {
                    e.Graphics.DrawImage(bmp, (this.Width - bmp.Width) / 2, (this.Height - bmp.Height)/2, bmp.Width, this.Height);
                }
                */
            }
            //e.Graphics.DrawImage(bmBack1, 150 + 70, 10, 900, 700);

        }
        public void RefreshForms()
        {

            //this.Invalidate();
            this.picturebox1.Image = Yugamiru.Properties.Resources.Mainpic;

            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_NONE)
                IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_up;
            else
                IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_on;
            /*if (m_OpenResultFlag) commented for GSP-464
                IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_on;
            else*/
            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_up;
            IDC_CloseBtn.Image = Yugamiru.Properties.Resources.end_up;
            IDC_SETTING_BTN.Image = Yugamiru.Properties.Resources.setting_up;
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(true);
        }
         //static bool m_OpenResultFlag = false; commented for GSP-464
        private void IDC_AnalysisBtn_Click(object sender, EventArgs e)
        {
            //m_OpenResultFlag = true;
            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_down;
            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_on;

            t.Interval = 100;
            t.Start();
            t.Tick += new EventHandler(timer_Tick);
            m_CLICK_MODE = "OPENRESULT";

        }

        private void Obj_EventToChangeResultView(object sender, EventArgs e)
        {
            FunctionToOpenSettingScreen(EventArgs.Empty);
        }

        public void IDD_BALANCELABO_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(this.Visible.ToString());
     /*       if (e.KeyCode == Keys.A && this.Visible == true)
            {
                //IDD_ABOUTBOX version = new IDD_ABOUTBOX();
                //Form fc = Application.OpenForms["IDD_ABOUTBOX"]; // to find form is already open

                if (Application.OpenForms.OfType<IDD_ABOUTBOX>().Count() == 1)
                    Application.OpenForms.OfType<IDD_ABOUTBOX>().First().Close();

               
                    IDD_ABOUTBOX version = new IDD_ABOUTBOX();
                    version.Show();
                
                    
                //write code for your shortcut action
            }*/
        }
    }
}
