﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace Yugamiru
{
    public class JointEditDoc
    {
        Image m_StandingMusclePicture;
        Image m_KneedownMusclePicuture;
        public string m_Image_Path;
        Bitmap m_Side_bmp;
        Bitmap m_Stand_bmp;
        Bitmap m_Knee_bmp;
        int mUpdateFlag = 0;

        Bitmap m_FlipImage;
        MuscleStatePatternDefinitionMgr m_MuscleStatePatternDefinitionMgr = new MuscleStatePatternDefinitionMgr();
        AngleActionSciptMgr m_AngleActionScriptMgr = new AngleActionSciptMgr();
        IntegerRangeDefinitionMgr m_MuscleRangeDefinitionMgr = new IntegerRangeDefinitionMgr();

        ResultActionScriptMgr m_ResultActionScriptMgrSide = new ResultActionScriptMgr();
        MuscleActionScriptMgr m_MuscleActionScriptMgr = new MuscleActionScriptMgr();

        RealValueRangeDefinitionMgr m_AngleRangeDefinitionMgr = new RealValueRangeDefinitionMgr();
        IDD_MeasurementDlg m_MeasurementDlg;
        IDD_MEASUREMENT_START_VIEW m_MeasurementStart;
        JointEditView m_JointEditView;
        IDD_BALANCELABO m_Balancelabo;// Initial Screen
        SettingView m_SettingView;
        OpenResult m_OpenResult;
        SideJointEditView m_SideJointEditView;
        IDD_BALANCELABO_DIALOG m_Idd_Balancelabo_Dialog;

        bool m_QRCode = false;
        Bitmap m_QRCodeImage;
        bool m_bDetail_info;        // Ú×î•ñ(‹ØŒQ–¼‚È‚Ç)‚Å•\Ž¦
        MyImage m_BalanceLaboImg;
        string m_strInstitutionName;   // Ž{Ý–¼
        int m_FontHeight;
        int m_FontWidth;

        double m_ImgMag;        //	Œ‹‰Ê‰æ–ÊŠg‘å—¦

        Font m_LogFont;      //			˜_—ƒtƒHƒ“ƒg.
        Color m_FontColor;   //			ƒtƒHƒ“ƒgƒJƒ‰[
        bool m_bOutline;        //					—ÖŠs
        Color m_OutlineColor;    //					—ÖŠsF

        int m_MarkerSize;   //			ƒ}[ƒJ[ƒTƒCƒY(”¼Œa)
        int m_iArrowLength;
        int m_iArrowWidth;
        int m_iArrowInvisible;
        int m_iLabelInvisible;
        int m_iFinalScreenMode;

        bool m_bMidLine;            // on/off
        int m_MidLineWidth;        // ‘¾‚³
        Color m_MidLineColor;        // F
        int m_MidLineStyle;        // ƒXƒ^ƒCƒ‹

        bool m_bStyleLine;      // on/off
        uint m_StyleLineWidth;  // ‘¾‚³
        Color m_StyleLineColor;  // F
        uint m_StyleLineStyle;  // ƒXƒ^ƒCƒ‹

        //ƒŒƒ|[ƒg—p‚Ì’èŒ`•¶
        string m_FixedComment;

        CCalcPostureProp m_CalcPostureProp; // Žp¨ŒvŽZ(ƒxƒ‹ƒg”FŽ¯)‚ÌÛ‚ÌŒvŽZðŒ
        SettingDataMgr m_SettingDataMgr = new SettingDataMgr();
        //SettingDataMgr m_SettingDataMgr5 = new SettingDataMgr();
        //SettingDataMgr m_SettingDataMgr2;
        /*CSettingDataMgr m_SettingDataMgr3;
        CSettingDataMgr m_SettingDataMgr4;
        CSettingDataMgr m_SettingDataMgr5;
        CRealValueRangeDefinitionMgr m_AngleRangeDefinitionMgr;
        CIntegerRangeDefinitionMgr m_MuscleRangeDefinitionMgr;
        CMuscleStatePatternDefinitionMgr m_MuscleStatePatternDefinitionMgr;
        CAngleActionScriptMgr m_AngleActionScriptMgr;
        CResultActionScriptMgr m_ResultActionScriptMgr;
        CResultActionScriptMgr m_ResultActionScriptMgrSide;
        CMuscleActionScriptMgr m_MuscleActionScriptMgr;*/

        public byte[] m_SideImageBytes;
        public byte[] m_FrontStandingImageBytes;
        public byte[] m_FrontKneedownImageBytes;
        CCalibInf m_CalibInf;
        public MyData m_BalanceLabData;
        //CMyImage m_BalanceLaboImg;
        bool m_bFailToReadInfFiles; // Ý’èƒtƒ@ƒCƒ‹‚Ì“Ç‚Ýž‚ÝŽ¸”s.

        string m_strINFFolderPath;         // INF files folder
        string m_strScoresheetFolderPath;
        string m_strImageFolderPath;
        bool m_bValidInitImageFolderItemIDList;
        //CItemIDListBuffer m_ItemIDListBufferImageFolderPath;
        string m_strInitImageExtName;      // ‰æ‘œ“Ç‚Ýž‚Ý•W€Šg’£Žq.
        string m_strSaveFilePath;          // ƒZ[ƒuƒtƒ@ƒCƒ‹‚Ö‚ÌƒpƒX.
        string m_Language = string.Empty;

        int m_iInputMode;                   // “ü—Íƒ‚[ƒh.
        int m_iMeasurementViewMode;         // ŒÂlî•ñ‰æ–Êƒ‚[ƒh.
        int m_iMeasurementStartViewMode;    // ‰æ‘œ“Çž‰æ–Êƒ‚[ƒh.
        int m_iJointEditViewMode;           // ŠÖßˆÊ’u•ÒW‰æ–Êƒ‚[ƒh.

        int m_iJudgeMode;                   // ‚ä‚ª‚Ý[‚éŽw”•]‰¿ƒ‚[ƒh.
        int m_dwStandingMarkerTouchFlags; // —§ˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
        int m_dwKneedownMarkerTouchFlags; // ‹üˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
        int m_dwSideMarkerTouchFlags;     // ‘¤–Êƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.

        public int m_iMarkerSizeType;

        int m_iDisplayArrow;
        int m_iDisplayCenterLine;
        int m_iDisplayLabel;
        int m_iDisplayScore;
        int m_iDisplayMuscleReport;
        int m_iDisplayCentroid;
        int m_iSettingMode;
        ResultActionScriptMgr m_ResultActionScriptMgr = new ResultActionScriptMgr();

        public FrontBodyPosition m_FrontBodyPositionStanding;
        public FrontBodyPosition m_FrontBodyPositionKneedown;
        public SideBodyPosition m_SideBodyPositionObj;

        int m_UniqueId;
        int m_ImportYGAFileMenuFlag = 0;
        int m_LanguageMenu = 1;
        public static DateTime qlsGetDate; //Added By Suhana
        int[] m_aiNewTrainingIDs = new int[Constants.DISPLAYED_NEWTRAINING_COUNT_MAX];

        public static CTrainingSelectConditionArray[] s_aTrainingSelectConditionArray;

        public static CTrainingSelectCondition[] s_aTrainingCondition00 =
        {
            new CTrainingSelectCondition
            {
                 m_iConditionID= Constants.TRAINING_SELECTCONDITIONID_NONE,
                 m_iPriority = 0
            }
        };
        public static CTrainingSelectCondition[] s_aTrainingCondition01 =
            {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_SHOULDERBAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
             };
        public static CTrainingSelectCondition[] s_aTrainingCondition02 =
           {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_SHOULDERBAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
            };
        public static CTrainingSelectCondition[] s_aTrainingCondition03 =
            {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_SHOULDERBAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HEADCENTER,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_BEND_BACKWARD,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FLATBACK,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
            };
        public static CTrainingSelectCondition[] s_aTrainingCondition04 =
            {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HIP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FRONTSIDE_UNBALANCED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
            };
        public static CTrainingSelectCondition[] s_aTrainingCondition05 =
           {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FRONTSIDE_UNBALANCED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
           };
        public static CTrainingSelectCondition[] s_aTrainingCondition06 =
            {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
            };
        public static CTrainingSelectCondition[] s_aTrainingCondition07 =
           {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
           };
        public static CTrainingSelectCondition[] s_aTrainingCondition08 =
           {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
           };
        public static CTrainingSelectCondition[] s_aTrainingCondition09 =
           {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FLATBACK,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
           };
        public static CTrainingSelectCondition[] s_aTrainingCondition10 =
    {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HIP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
    };

        public static CTrainingSelectCondition[] s_aTrainingCondition11 =
            {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HIP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
            };

        public static CTrainingSelectCondition[] s_aTrainingCondition12 =
     {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
     };

        public static CTrainingSelectCondition[] s_aTrainingCondition13 =
    {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
    };
        public static CTrainingSelectCondition[] s_aTrainingCondition14 =
 {

                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
 };
        public static CTrainingSelectCondition[] s_aTrainingCondition15 =
        {

                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
        };


        public static CTrainingSelectCondition[] s_aTrainingCondition16 =
 {

                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
 };
        public static CTrainingSelectCondition[] s_aTrainingCondition17 =
  {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HIP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
  };
        public static CTrainingSelectCondition[] s_aTrainingCondition18 =
{
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_HIP,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
};


        public static CTrainingSelectCondition[] s_aTrainingCondition19 =
 {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
 };

        public static CTrainingSelectCondition[] s_aTrainingCondition20 =
{
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
};
        public static CTrainingSelectCondition[] s_aTrainingCondition21 =
{
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
};
        public static CTrainingSelectCondition[] s_aTrainingCondition22 =
      {
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_XLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_OLEGGED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FRONTSIDE_UNBALANCED,
                    m_iPriority = 0
                },
                new CTrainingSelectCondition
                {
                    m_iConditionID = Constants.TRAINING_SELECTCONDITIONID_NONE,
                    m_iPriority = 0
                },
      };
        //private readonly int m_dwStandingMarkerTouchFlags;

        public JointEditDoc()
        {
            m_BalanceLaboImg = new MyImage();

            m_CalibInf = new CCalibInf();
            m_CalcPostureProp = new CCalcPostureProp();
            m_BalanceLabData = new MyData();

            s_aTrainingSelectConditionArray = new CTrainingSelectConditionArray[Constants.TRAININGID_MAX];

            // s_aTrainingCondition00 = new CTrainingSelectCondition[0];
            // s_aTrainingCondition00 = { };
            s_aTrainingSelectConditionArray[0] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition00
            };
            s_aTrainingSelectConditionArray[1] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition01
            };
            s_aTrainingSelectConditionArray[2] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition02
            };
            s_aTrainingSelectConditionArray[3] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition03
            };
            s_aTrainingSelectConditionArray[4] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition04
            };
            s_aTrainingSelectConditionArray[5] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition05
            };
            s_aTrainingSelectConditionArray[6] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition06
            };
            s_aTrainingSelectConditionArray[7] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition07
            };
            s_aTrainingSelectConditionArray[8] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition08
            };
            s_aTrainingSelectConditionArray[9] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition09
            };
            s_aTrainingSelectConditionArray[10] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition10
            };
            s_aTrainingSelectConditionArray[11] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition11
            };
            s_aTrainingSelectConditionArray[12] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition12
            };
            s_aTrainingSelectConditionArray[13] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition13
            };
            s_aTrainingSelectConditionArray[14] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition14
            };
            s_aTrainingSelectConditionArray[15] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition15
            };
            s_aTrainingSelectConditionArray[16] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition16
            };
            s_aTrainingSelectConditionArray[17] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition17
            };
            s_aTrainingSelectConditionArray[18] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition18
            };
            s_aTrainingSelectConditionArray[19] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition19
            };
            s_aTrainingSelectConditionArray[20] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition20
            };
            s_aTrainingSelectConditionArray[21] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition21
            };
            s_aTrainingSelectConditionArray[22] = new CTrainingSelectConditionArray
            {
                m_pTrainingSelectCondition = s_aTrainingCondition22
            };


            m_bFailToReadInfFiles = false;
            m_iInputMode = Constants.INPUTMODE_NONE;
            m_iMeasurementStartViewMode = Constants.MEASUREMENTSTARTVIEWMODE_NONE;
            m_iJointEditViewMode = Constants.JOINTEDITVIEWMODE_NONE;
            m_bValidInitImageFolderItemIDList = false;

            m_iJudgeMode = 0;
            m_dwStandingMarkerTouchFlags = 0;
            m_dwKneedownMarkerTouchFlags = 0;
            m_dwSideMarkerTouchFlags = 0;

            m_iMarkerSizeType = 2;
            m_iDisplayArrow = 1;
            m_iDisplayCenterLine = 1;
            m_iDisplayLabel = 1;
            m_iDisplayScore = 1;
            m_iDisplayMuscleReport = 1;
            m_iDisplayCentroid = 1;

            int i = 0;
            for (i = 0; i < Constants.DISPLAYED_NEWTRAINING_COUNT_MAX; i++)
            {
                m_aiNewTrainingIDs[i] = Constants.TRAININGID_NONE;
            }
        }
        public bool IsValidInitImageFolderItemIDList()
        {
            return m_bValidInitImageFolderItemIDList;
        }
        public bool OnNewDocument()
        {


            // TODO: ‚±‚ÌˆÊ’u‚ÉÄ‰Šú‰»ˆ—‚ð’Ç‰Á‚µ‚Ä‚­‚¾‚³‚¢B
            // (SDI ƒhƒLƒ…ƒƒ“ƒg‚Í‚±‚ÌƒhƒLƒ…ƒƒ“ƒg‚ðÄ—˜—p‚µ‚Ü‚·B)
            /* m_LogFont.lfHeight = -16;
             m_LogFont.lfWidth = 0;
             m_LogFont.lfEscapement = 0;
             m_LogFont.lfOrientation = 0;
             m_LogFont.lfWeight = FW_DONTCARE;
             m_LogFont.lfItalic = FALSE;
             m_LogFont.lfUnderline = FALSE;
             m_LogFont.lfStrikeOut = FALSE;
             m_LogFont.lfCharSet = SHIFTJIS_CHARSET;
             m_LogFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
             m_LogFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
             m_LogFont.lfQuality = DRAFT_QUALITY;
             m_LogFont.lfPitchAndFamily = DEFAULT_PITCH;
             strcpy(&(m_LogFont.lfFaceName[0]), "‚l‚r ƒSƒVƒbƒN");*/

            m_ImgMag = 1;

            m_bStyleLine = true;
            m_StyleLineWidth = 1;
            m_StyleLineColor = Color.FromArgb(255, 255, 0);
            //m_StyleLineStyle = Color.PS_SOLID;

            m_bMidLine = true;
            m_MidLineWidth = 1;
            m_MidLineColor = Color.FromArgb(255, 0, 0);
            //m_MidLineStyle = PS_SOLID;

            m_MarkerSize = 12;
            m_iArrowLength = 0;
            m_iArrowWidth = 0;
            m_iArrowInvisible = 0;
            m_iLabelInvisible = 0;
            m_Language = "English";

            m_CalibInf.Initialize();

            m_BalanceLaboImg.AllocStandingImage();
            m_BalanceLaboImg.AllocKneedownImage();
            m_BalanceLaboImg.AllocSideImage();
            m_BalanceLaboImg.CreateBmpInfo(1024 * 1280 * 3, 1024, 1280);
            m_CalibInf.Initialize();
            LoadSetupFile(@"Resources\", "setting.ini");
            LoadSetup4File();
            m_BalanceLabData.InitBodyBalance(1024, 1280, m_CalibInf, m_CalcPostureProp);
            //LoadSetupFile("test", "test");

            string strModulePath = string.Empty;
            GetModuleFilePath(strModulePath);

            m_strINFFolderPath = strModulePath;
            /*   m_strScoresheetFolderPath = strModulePath + "Scoresheet\\";
                       m_strImageFolderPath = strModulePath + "Image\\";
            
                       CString strErrFilePath;
                       LoadSetupFile(strModulePath, "setting.ini");
                       LoadSetup3File();
                       LoadSetup4File();
                       LoadSetup5File();
                       */


            m_AngleRangeDefinitionMgr.ReadFromFile(@"Resources\", "AngleRange.csv", null);
            m_MuscleRangeDefinitionMgr.ReadFromFile(@"Resources\", "MuscleRange.csv", null);
            m_MuscleStatePatternDefinitionMgr.ReadFromFile(@"Resources\", "MuscleStatePattern.CSV", null);
            m_AngleActionScriptMgr.ReadFromFile(@"Resources\", "AngleAction.inf", null);
            m_ResultActionScriptMgr.ReadFromFile(@"Resources\", "ResultAction.inf", null);
            m_ResultActionScriptMgrSide.ReadFromFile(@"Resources\", "ResultActionSide.inf", null /*strErrFilePath*/ );
            m_MuscleActionScriptMgr.ReadFromFile(@"Resources\", "MuscleAction.inf", null);
            m_AngleActionScriptMgr.ResolveRange(m_AngleRangeDefinitionMgr, null);
            m_ResultActionScriptMgr.ResolveRange(m_AngleRangeDefinitionMgr, null);
            m_ResultActionScriptMgrSide.ResolveRange(m_AngleRangeDefinitionMgr, null);
            m_MuscleActionScriptMgr.ResolveRange(m_MuscleRangeDefinitionMgr, null /*strErrFilePath*/ );




            return true;
        }
        public void CalcStandingMuscleColor(MuscleColorInfo MuscleColorInfoStanding)
        {

            FrontBodyAngle angleStanding = new FrontBodyAngle();

            CalcBodyAngleStanding(ref angleStanding);
            SideBodyAngle angleSide = new SideBodyAngle();

            CalcBodyAngleSide(ref angleSide);
            MuscleStateInfo MuscleStateInfoStanding = new MuscleStateInfo();
            m_AngleActionScriptMgr.Execute(MuscleStateInfoStanding, angleStanding, angleSide,
            m_MuscleStatePatternDefinitionMgr);
            m_MuscleActionScriptMgr.Execute(MuscleColorInfoStanding, MuscleStateInfoStanding);
        }
        public void CalcKneedownMuscleColor(MuscleColorInfo MuscleColorInfoKneedown)
        {

            FrontBodyAngle angleKneedown = new FrontBodyAngle();

            CalcBodyAngleKneedown(ref angleKneedown);
            SideBodyAngle angleSide = new SideBodyAngle();

            CalcBodyAngleSide(ref angleSide);
            MuscleStateInfo MuscleStateInfoKneedown = new MuscleStateInfo();
            m_AngleActionScriptMgr.Execute(MuscleStateInfoKneedown, angleKneedown, angleSide,
            m_MuscleStatePatternDefinitionMgr);
            m_MuscleActionScriptMgr.Execute(MuscleColorInfoKneedown, MuscleStateInfoKneedown);
        }
        public void GetModuleFilePath(string strModulePath)
        {

            strModulePath = @"Resources\";
            //strModulePath = @"Resources\ResultAction.inf";
            //var t = File.ReadAllLines(strModulePath);

        }


        public int GetInputMode()
        {
            return m_iInputMode;
        }

        public void SetInputMode(int iInputMode)
        {
            m_iInputMode = iInputMode;
        }
        public int GetSettingMode()
        {
            return m_iSettingMode;
        }

        public void SetSettingMode(int iSettingMode)
        {
            m_iSettingMode = iSettingMode;
        }


        /*  public void AllocSideImage()
          {
              m_BalanceLaboImg.AllocSideImage();
          }*/
        public void AllocSideImage(byte[] SideImage)
        {
            m_SideImageBytes = SideImage;
        }
        public void AllocStandingImage(byte[] StandingImage)
        {
            m_FrontStandingImageBytes = StandingImage;
        }
        public void AllocKneedownImage(byte[] KneedownImage)
        {
            m_FrontKneedownImageBytes = KneedownImage;
        }

        public int GetMeasurementViewMode()
        {
            return m_iMeasurementViewMode;
        }

        public void SetMeasurementViewMode(int iMeasurementViewMode)
        {
            m_iMeasurementViewMode = iMeasurementViewMode;            
        }

        public int GetMeasurementStartViewMode()
        {           
            return m_iMeasurementStartViewMode;
        }
        //Added By Suhana For GSP 868 and GSP 869
        public void showStatus()
        {
            #region TrialBox,Shopping Cart
            string comp_key = "";
            string license_key = "";

            string computerID = WebComCation.keyRequest.GetComputerID();
            QlmLicenseLib.QlmLicense qls = new QlmLicenseLib.QlmLicense();
            qls.DefineProduct(WebComCation.ProductInfo.ProductID, WebComCation.ProductInfo.ProductName, WebComCation.ProductInfo.ProductVersionMajor
               , WebComCation.ProductInfo.ProductVersionMinor, WebComCation.ProductInfo.ProductEncryptionKey, WebComCation.ProductInfo.ProductPersistencyKey);
            qls.PublicKey = WebComCation.ProductInfo.ProductEncryptionKey;
            qls.ReadKeys(ref license_key, ref comp_key);

            qls.ValidateLicenseEx(comp_key, WebComCation.keyRequest.GetComputerID());
            qls.GetStatus();
            qlsGetDate = qls.ExpiryDate;
            int dRenewDate = ValidateDates();
            int ReadRenewDates = dRenewDate;
            //  int ReadRenewDates = dRenewDate;
            if (ReadRenewDates <= 30)
            {
                if (qls.DaysLeft <= 15)
                {
                    m_Idd_Balancelabo_Dialog.trialExpiryBox.Text = qls.DaysLeft.ToString() + " " + "days left";
                    m_Idd_Balancelabo_Dialog.trialExpiryBox.Visible = true;
                  //  m_Idd_Balancelabo_Dialog.button1.Visible = true;
                }
                else
                {
                    m_Idd_Balancelabo_Dialog.trialExpiryBox.Visible = false;
                  //  m_Idd_Balancelabo_Dialog.button1.Visible = false;
                }

            }
            else
            {
                m_Idd_Balancelabo_Dialog.trialExpiryBox.Visible = false;
                m_Idd_Balancelabo_Dialog.button1.Visible = false;

            }



            #endregion
            //Added By suhana For GSP 868
        }





        public int ValidateDates()
        {
            string renewDate = WebComCation.LicenseValidator.showRenewDate;
            if (renewDate != null)
            {
                DateTime ddRenew = DateTime.Parse(renewDate).Date;
                DateTime GetQLSExpiryDays = qlsGetDate;
                TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                int getD = (int)leftTotalDays.TotalDays;



                return getD;
            }
            else
            {

                string showActivationDate = WebComCation.Utility.GetSavedStamp();
                if (showActivationDate == "")
                {
                    return 0;
                }
                else
                {
                    DateTime ddRenew = DateTime.Parse(showActivationDate).Date;
                    DateTime GetQLSExpiryDays = qlsGetDate;
                    TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                    int getD = (int)leftTotalDays.TotalDays;

                    return getD;
                }
            }
        }
        //Added By Suhana For GSP 868 and GSP 869
        public void SetMeasurementStartViewMode(int iMeasurementStartViewMode)
        {            
            m_iMeasurementStartViewMode = iMeasurementStartViewMode;           
        }

        public int GetJointEditViewMode()
        {
            return m_iJointEditViewMode;
        }

        public void SetJointEditViewMode(int iJointEditViewMode)
        {
            m_iJointEditViewMode = iJointEditViewMode;
        }

        public void ClearMakerTouchFlag()
        {
            m_dwStandingMarkerTouchFlags = 0;   // —§ˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
            m_dwKneedownMarkerTouchFlags = 0;   // ‹üˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
            m_dwSideMarkerTouchFlags = 0;       // ‘¤–Êƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
        }
        public void InitBodyBalance()
        {
            int iWidth = m_BalanceLaboImg.GetWidth();
            int iHeight = m_BalanceLaboImg.GetHeight();
            m_BalanceLabData.InitBodyBalance(iWidth, iHeight, m_CalibInf, m_CalcPostureProp);
        }
        public bool ClearStandingImage()
        {
            return m_BalanceLaboImg.ClearStandingImage();
        }

        public bool ClearKneedownImage()
        {
            return m_BalanceLaboImg.ClearKneedownImage();
        }

        public bool ClearSideImage()
        {
            return m_BalanceLaboImg.ClearSideImage();
        }
        public void SetDataMeasurementTime(string pchMeasurementTime)
        {

            m_BalanceLabData.SetMeasurementTime(pchMeasurementTime);
        }
        public void SetSaveFilePath(string pchSaveFilePath)
        {
            m_strSaveFilePath = pchSaveFilePath;
        }
        public void ChangeToMeasurementView()
        {
            /*  CMainFrame* pMainFrame = DYNAMIC_DOWNCAST(CMainFrame, AfxGetMainWnd());
              if (pMainFrame == NULL)
              {
                  return;
              }
              pMainFrame->ChangeView(RUNTIME_CLASS(CMeasurementView));*/
            /*IDD_MeasurementDlg MeasurementDlg = new IDD_MeasurementDlg();
            MeasurementDlg.Show();*/


        }
        public bool CreateBmpInfo(int iSize, int iWidth, int iHeight)
        {
            return m_BalanceLaboImg.CreateBmpInfo(iSize, iWidth, iHeight);
        }
        public void GetDataDoB(ref string strY, ref string strM, ref string strD)
        {
            m_BalanceLabData.GetDoB(ref strY, ref strM, ref strD);
        }
        public string GetDataMeasurementTime()
        {
            return m_BalanceLabData.GetMeasurementTime();
        }

        public void GetDataAcqDate(ref string strY, ref string strM, ref string strD, ref string strT)
        {
            m_BalanceLabData.GetAcqDate(ref strY, ref strM, ref strD, ref strT);
        }

        public int GetBenchmarkDistance()
        {
            return m_BalanceLabData.GetBenchmarkDistance();
        }

        public void SetDataID(string pchID)
        {
            m_BalanceLabData.SetID(pchID);
        }

        public void SetDataName(string pchName)
        {
            m_BalanceLabData.SetName(pchName);
        }

        public void SetDataGender(int iGender)
        {
            m_BalanceLabData.SetGender(iGender);
        }


        public void SetDataHeight(float fHeight)
        {
            m_BalanceLabData.SetHeight(fHeight);
        }

        public void SetDataDoB(string strY, string strM, string strD)
        {
            m_BalanceLabData.SetDoB(strY, strM, strD);
        }

        public void SetDataComment(string pchComment)
        {
            m_BalanceLabData.SetComment(pchComment);
        }

        public void SetBenchmarkDistance(int iBenchmarkDistance)
        {
            m_BalanceLabData.SetBenchmarkDistance(iBenchmarkDistance);
        }
        public double GetDataHeight()
        {
            return m_BalanceLabData.GetHeight();
        }
        public int GetDataGender()
        {
            return m_BalanceLabData.GetGender();
        }
        public void GetInstitutionName(String strInstitutionName)
        {

            strInstitutionName = m_strInstitutionName;
        }
        public byte[] GetSideImagePointer()
        {
            return m_BalanceLaboImg.GetSideImagePointer();
        }

        public string GetDataID()
        {
            return m_BalanceLabData.GetID();
        }

        public string GetDataName()
        {
            return m_BalanceLabData.GetName();
        }


        public string GetDataComment()
        {
            return m_BalanceLabData.GetComment();
        }

        public int GetMarkerSize()
        {
            if (m_iMarkerSizeType == 2)
            {
                m_MarkerSize = 24;//18;//12;
            }
            if (m_iMarkerSizeType == 1)
            {
                m_MarkerSize = 18;//15;//9;
            }
            if (m_iMarkerSizeType == 0)
            {
                m_MarkerSize = 14;//12;//6;
            }
            return m_MarkerSize;
        }

        public void GetStandingFrontBodyPosition(ref FrontBodyPosition FrontBodyPositionStanding)
        {

            m_BalanceLabData.GetStandingFrontBodyPosition(ref FrontBodyPositionStanding);
        }

        public void GetKneedownFrontBodyPosition(ref FrontBodyPosition FrontBodyPositionKneedown)
        {
            m_BalanceLabData.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedown);
        }

        public void GetSideBodyPosition(ref SideBodyPosition SideBodyPosition)
        {
            m_BalanceLabData.GetSideBodyPosition(ref SideBodyPosition);

        }
        public double GetBelt2KneeRatio()
        {
            return m_CalcPostureProp.GetBelt2KneeRatio();
        }
        public double GetKneeRatioError()
        {
            return m_CalcPostureProp.GetKneeRatioError();
        }
        public bool GetStandingMarkerTouchFlag(int iEditPointID)
        {
            int i = m_dwStandingMarkerTouchFlags & (1 << iEditPointID);
            if (i == 0)
                return false;
            else
                return true;
            //return (m_dwStandingMarkerTouchFlags & (1 << iEditPointID));
            // —§ˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.

        }

        public bool GetKneedownMarkerTouchFlag(int iEditPointID)
        {
            //return ( m_dwKneedownMarkerTouchFlags & ( 1 << iEditPointID ));	// ‹üˆÊƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
            int i = m_dwKneedownMarkerTouchFlags & (1 << iEditPointID);
            if (i == 0)
                return false;
            else
                return true;
        }

        public bool GetSideMarkerTouchFlag(int iEditPointID)
        {
            //return ( m_dwSideMarkerTouchFlags & ( 1 << iEditPointID ));		// ‘¤–Êƒ}[ƒJ[‚ð•ÒW‚µ‚½‚©.
            int i = m_dwSideMarkerTouchFlags & (1 << iEditPointID);
            if (i == 0)
                return false;
            else
                return true;
        }



        public void SetStandingMarkerTouchFlag(int iEditPointID)
        {
            m_dwStandingMarkerTouchFlags |= (1 << iEditPointID);
            // m_dwStandingMarkerTouchFlags = iEditPointID;

        }

        public void SetKneedownMarkerTouchFlag(int iEditPointID)
        {
            m_dwKneedownMarkerTouchFlags |= (1 << iEditPointID);
        }

        public void SetSideMarkerTouchFlag(int iEditPointID)
        {
            m_dwSideMarkerTouchFlags |= (1 << iEditPointID);
        }

        public int GetMarkerSizeType()
        {
            return m_iMarkerSizeType;
        }

        public int GetDisplayArrow()
        {
            return m_iDisplayArrow;
        }

        public int GetDisplayCenterLine()
        {
            return m_iDisplayCenterLine;
        }

        public int GetDisplayLabel()
        {
            return m_iDisplayLabel;
        }

        public int GetDisplayScore()
        {
            return m_iDisplayScore;
        }

        public int GetDisplayMuscleReport()
        {
            return m_iDisplayMuscleReport;
        }

        public int GetDisplayCentroid()
        {
            return m_iDisplayCentroid;
        }
        public int GetCentroidLineWidth()
        {
            return GetMidLineWidth();   // ’†Sü‚Æ‹¤’Ê‚É‚·‚é.
        }

        public Color GetCentroidLineColor()
        {
            return Color.Red;
        }
        public int GetCentroidLineStyle()
        {
            return GetMidLineStyle();   // ’†Sü‚Æ‹¤’Ê‚É‚·‚é
        }

        public void SetMarkerSizeType(int iMarkerSizeType)
        {
            m_iMarkerSizeType = iMarkerSizeType;
        }

        public void SetDisplayArrow(int iDisplayArrow)
        {
            m_iDisplayArrow = iDisplayArrow;
        }

        public void SetDisplayCenterLine(int iDisplayCenterLine)
        {
            m_iDisplayCenterLine = iDisplayCenterLine;
        }

        public void SetDisplayLabel(int iDisplayLabel)
        {
            m_iDisplayLabel = iDisplayLabel;
        }

        public void SetDisplayScore(int iDisplayScore)
        {
            m_iDisplayScore = iDisplayScore;
        }

        public void SetDisplayMuscleReport(int iDisplayMuscleReport)
        {
            m_iDisplayMuscleReport = iDisplayMuscleReport;
        }

        public void SetDisplayCentroid(int iDisplayCentroid)
        {
            m_iDisplayCentroid = iDisplayCentroid;
        }

        public int GetJudgeMode()
        {
            return m_iJudgeMode;
        }
        public double GetHipMko()
        {
            return m_CalcPostureProp.GetHipMko();
        }


        public double GetBelt2HipRatio()
        {
            return m_CalcPostureProp.GetBelt2HipRatio();
        }
        public void SetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
        {
            m_BalanceLabData.SetStandingFrontBodyPosition(FrontBodyPositionStanding);
        }
        public void SetStandingFrontBodyPositionObj(FrontBodyPosition FrontBodyPositionStanding)
        {
            m_FrontBodyPositionStanding = FrontBodyPositionStanding;
        }
        public void SetKneedownFrontBodyPositionObj(FrontBodyPosition FrontBodyPositionKneedown)
        {
            m_FrontBodyPositionKneedown = FrontBodyPositionKneedown;
        }
        public void SetSideBodyPosition(SideBodyPosition SideBodyPosition)
        {
            m_BalanceLabData.SetSideBodyPosition(SideBodyPosition);
        }
        public void SetSideBodyPositionObj(SideBodyPosition SideBodyPosition)
        {
            m_SideBodyPositionObj = SideBodyPosition;
        }
        public void SetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
        {
            m_BalanceLabData.SetKneedownFrontBodyPosition(FrontBodyPositionKneedown);
        }
        public void SetQRCodeFlag(bool flag)
        {
            m_QRCode = flag;
        }
        public bool GetQRCodeFlag()
        {
            return m_QRCode;
        }
        public void SetQRCodeImage(Bitmap Image)
        {
            m_QRCodeImage = Image;
        }
        public Bitmap GetQRCodeImage()
        {
            return m_QRCodeImage;
        }
        public bool IsValidStyleLine()
        {
            return m_bStyleLine;
        }
        public bool IsValidCentroidLine()
        {
            if (m_iDisplayCentroid == 1)
                return true;
            else
                return false;
        }

        public uint GetStyleLineWidth()
        {
            return m_StyleLineWidth;
        }

        public Color GetStyleLineColor()
        {
            return m_StyleLineColor;
        }

        public uint GetStyleLineStyle()
        {
            return m_StyleLineStyle;
        }
        public int GetDataVersion()
        {
            return m_BalanceLabData.GetVersion();
        }
        public int GetShoulderOffset0()
        {
            return m_CalcPostureProp.GetShoulderOffset(0);
        }

        public int GetShoulderOffset1()
        {
            return m_CalcPostureProp.GetShoulderOffset(1);
        }

        public int GetShoulderOffset2()
        {
            return m_CalcPostureProp.GetShoulderOffset(2);
        }

        public bool IsValidMidLine()
        {
            if (m_iDisplayCenterLine <= 0)
            {
                return false;
            }
            return m_bMidLine;
        }

        public int GetMidLineWidth()
        {
            return m_MidLineWidth;
        }

        public Color GetMidLineColor()
        {
            return m_MidLineColor;
        }

        public int GetMidLineStyle()
        {
            return m_MidLineStyle;
        }

        public Color GetFontColor()
        {
            return m_FontColor;
        }

        public bool IsValidOutline()
        {
            return m_bOutline;
        }

        public Color GetOutlineColor()
        {
            return m_OutlineColor;
        }
        public void CalcBodyAngleStanding(ref FrontBodyAngle angleStanding)
        {

            m_BalanceLabData.CalcBodyAngleStanding(ref angleStanding, m_CalibInf.GetCameraAngle());
        }

        public void CalcBodyAngleKneedown(ref FrontBodyAngle angleKneedown)
        {
            m_BalanceLabData.CalcBodyAngleKneedown(ref angleKneedown, m_CalibInf.GetCameraAngle());
        }

        public void CalcBodyAngleSide(ref SideBodyAngle angleSide)
        {
            m_BalanceLabData.CalcBodyAngleSide(ref angleSide, m_CalibInf.GetCameraAngle());
        }
        public void CalcStandingFrontBodyResultData(ref FrontBodyResultData resultStanding)
        {

            m_BalanceLabData.CalcStandingFrontBodyResultData(ref resultStanding, m_CalibInf.GetCameraAngle(), m_ResultActionScriptMgr);
        }

        public void CalcKneedownFrontBodyResultData(ref FrontBodyResultData resultKneedown)
        {
            m_BalanceLabData.CalcKneedownFrontBodyResultData(ref resultKneedown, m_CalibInf.GetCameraAngle(), m_ResultActionScriptMgr);
        }
        public void GetFixedComment(ref string strFixedComment)
        {

            strFixedComment = m_FixedComment;
        }
        public bool IsAllBodyPositionDetected()
        {
            return false;
            //return m_BalanceLabData.IsDetected();
        }
        public void CalcSideBodyResultData(ref SideBodyResultData resultSide)
        {

            m_BalanceLabData.CalcSideBodyResultData(ref resultSide, m_CalibInf.GetCameraAngle(), m_ResultActionScriptMgrSide);
        }
        public bool IsScoresheetDetail()
        {
            return m_bDetail_info;
        }
        public double GetImgMag()
        {
            return m_ImgMag;
        }
        public int GetArrowLength()
        {
            return m_iArrowLength;
        }

        public int GetArrowWidth()
        {
            return m_iArrowWidth;
        }

        public int IsArrowInvisible()
        {
            if (m_iDisplayArrow <= 0)
            {
                return 1;
            }
            return m_iArrowInvisible;
        }

        public int IsLabelInvisible()
        {
            if (m_iDisplayLabel <= 0)
            {
                return 1;
            }
            return m_iLabelInvisible;
        }

        public void SetLanguage(string str)
        {
            m_Language = str;
        }
        public string GetLanguage()
        {
            return m_Language;
        }
        public void SetFlipImage(Bitmap image)
        {
            m_FlipImage = image;
        }
        public Bitmap GetFlipImage()
        {
            return m_FlipImage;
        }
        public void SetFinalScreenMode(int FinalScreenMode)
        {
            m_iFinalScreenMode = FinalScreenMode;
        }
        public int GetFinalScreenMode()
        {
            return m_iFinalScreenMode;
        }
        public void SetSideClipImage(Bitmap bmp)
        {
            m_Side_bmp = bmp;
        }
        public Bitmap GetSideClipImage()
        {
            return m_Side_bmp;
        }
        public void SetStandClipImage(Bitmap bmp)
        {
            m_Stand_bmp = bmp;
        }
        public Bitmap GetStandClipImage()
        {
            return m_Stand_bmp;
        }
        public void SetKneeClipImage(Bitmap bmp)
        {
            m_Knee_bmp = bmp;
        }
        public Bitmap GetKneeClipImage()
        {
            return m_Knee_bmp;
        }
        public void SetStandingMusclePicture(Image img)
        {
            m_StandingMusclePicture = img;
        }
        public Image GetStandingMusclePicture()
        {
            return m_StandingMusclePicture;
        }
        public void SetKneedownMusclePicture(Image img)
        {
            m_KneedownMusclePicuture = img;
        }
        public Image GetKneedownMusclePicture()
        {
            return m_KneedownMusclePicuture;
        }
        public bool LoadSetup2File()
        {
            if (!File.Exists(Constants.programdata_path + "autosave.txt"))
            {
                if (!m_SettingDataMgr.ReadFromFile(@"Resources\", "autosave.txt"))
                {
                    return false;
                }
                File.Copy(@"Resources\autosave.txt",
                       Constants.programdata_path + "autosave.txt");
            }
            else if (!m_SettingDataMgr.ReadFromFile(Constants.programdata_path, "autosave.txt"))
            {
                return false;
            }

            string strValue = string.Empty;
            if (m_SettingDataMgr.GetValriableValue("BENCHMARK_DISTANCE", ref strValue))
            {

                int iValue;
                try
                {
                    iValue = int.Parse(strValue);
                }
                catch
                {
                    iValue = 80;//--Added by Rajnish For GSP-769--//
                }
                m_BalanceLabData.SetBenchmarkDistance(iValue);
            }
            return true;
        }

        public bool LoadSetupFile(string pchFolderName, string pchFileName)
        {
            // Ý’èƒtƒ@ƒCƒ‹‚Å“Ç‚Ýž‚Ü‚ê‚È‚©‚Á‚½ê‡‚É”õ‚¦‚ÄAƒfƒtƒHƒ‹ƒg’l‚ÌÝ’è.	
            m_strInstitutionName = string.Empty;
            m_ImgMag = 1.0;
            /*	m_LogFont.lfHeight					= -16;
                m_LogFont.lfWidth					= 0;
                m_LogFont.lfEscapement				= 0;
                m_LogFont.lfOrientation				= 0;
                m_LogFont.lfWeight					= FW_DONTCARE;
                m_LogFont.lfItalic					= FALSE;
                m_LogFont.lfUnderline				= FALSE;
                m_LogFont.lfStrikeOut				= FALSE;
                m_LogFont.lfCharSet					= SHIFTJIS_CHARSET;
                m_LogFont.lfOutPrecision			= OUT_DEFAULT_PRECIS;
                m_LogFont.lfClipPrecision			= CLIP_DEFAULT_PRECIS;
                m_LogFont.lfQuality					= DRAFT_QUALITY;
                m_LogFont.lfPitchAndFamily			= DEFAULT_PITCH;

                strcpy( &(m_LogFont.lfFaceName[0]), "‚l‚r ƒSƒVƒbƒN" );*/
            m_FontColor = Color.FromArgb(0, 0, 0);
            m_bOutline = false;
            m_OutlineColor = Color.FromArgb(255, 255, 255);
            m_bMidLine = true;
            m_MidLineWidth = 1;
            m_MidLineColor = Color.FromArgb(255, 0, 0);
            m_MidLineStyle = Constants.PS_SOLID;
            m_bStyleLine = true;
            m_StyleLineWidth = 4;
            m_StyleLineColor = Color.FromArgb(255, 255, 0);
            m_StyleLineStyle = Constants.PS_SOLID;
            m_bDetail_info = true;
            m_MarkerSize = 12;//Constants.MARKER_SIZE_M;
            m_FixedComment = string.Empty;
            m_CalcPostureProp.SetHipMko(0.20);
            m_CalcPostureProp.SetBelt2KneeRatio(0.45);
            m_CalcPostureProp.SetBelt2HipRatio(0.135);
            m_CalcPostureProp.SetKneeRatioError(-0.05);
            m_CalcPostureProp.SetShoulderOffset(0, 20);
            m_CalcPostureProp.SetShoulderOffset(1, 30);
            m_CalcPostureProp.SetShoulderOffset(2, 20);
            //return true;

            if (!m_SettingDataMgr.ReadFromFile(pchFolderName, pchFileName))
            {
                return false;
            }

            int iValue = 0;
            double dValue = 0.0;
            string strValue = string.Empty;
            if (m_SettingDataMgr.GetValriableValue("INSTITUTION_NAME", ref strValue))
            {
                m_strInstitutionName = strValue;
            }
            if (m_SettingDataMgr.GetValriableValue("IMAGE_SCALE", ref strValue))
            {
                m_ImgMag = Convert.ToDouble(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_HEIGHT", ref strValue))
            {
                /*m_LogFont.lfHeight = Convert.ToInt32(strValue ); */
                m_FontHeight = Convert.ToInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_WIDTH", ref strValue))
            {
                /*m_LogFont.lfWidth = atoi(strValue );*/
                m_FontWidth = Convert.ToInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_ESCAPEMENT", ref strValue))
            {
                /*m_LogFont.lfEscapement = atoi(strValue );*/
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_ORIENTATION", ref strValue))
            {
                //m_LogFont.lfOrientation = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_WEIGHT", ref strValue))
            {
                //m_LogFont.lfWeight = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_ITALIC", ref strValue))
            {
                //m_LogFont.lfItalic = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_UNDERLINE", ref strValue))
            {
                //m_LogFont.lfUnderline = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_STRIKEOUT", ref strValue))
            {
                //m_LogFont.lfStrikeOut = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_CHARSET", ref strValue))
            {
                //m_LogFont.lfCharSet = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_OUTPRECISION", ref strValue))
            {
                //m_LogFont.lfOutPrecision = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_CLIPPRECISION", ref strValue))
            {
                //m_LogFont.lfClipPrecision = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_QUALITY", ref strValue))
            {
                //m_LogFont.lfQuality = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_PITCHANDFAMILY", ref strValue))
            {
                //m_LogFont.lfPitchAndFamily = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_FACENAME", ref strValue))
            {

                /*memset( &(m_LogFont.lfFaceName[0]), 0, sizeof(m_LogFont.lfFaceName) );
                int iCopyLength = strlen(strValue);
                if ( iCopyLength > sizeof(m_LogFont.lfFaceName) - 1 ){
                    iCopyLength = sizeof(m_LogFont.lfFaceName) - 1;
                }
                int i = 0;
                for( i = 0; i<iCopyLength; i++ ){
                    m_LogFont.lfFaceName[i] = strValue[i];
                }*/
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_COLOR", ref strValue))
            {
                //m_FontColor = atoi(strValue );	
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_OUTLINE", ref strValue))
            {
                //m_bOutline = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("FONT_OUTLINECOLOR", ref strValue))
            {
                //m_OutlineColor = atoi(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("MIDLINE_VALID", ref strValue))
            {
                m_bMidLine = strValue.Equals("1") ? true : false;
            }
            if (m_SettingDataMgr.GetValriableValue("MIDLINE_WIDTH", ref strValue))
            {
                m_MidLineWidth = Convert.ToInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("MIDLINE_COLOR", ref strValue))
            {
                m_MidLineColor = Color.FromName(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("MIDLINE_STYLE", ref strValue))
            {
                m_MidLineStyle = Convert.ToInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("STYLELINE_VALID", ref strValue))
            {
                m_bStyleLine = strValue.Equals("1") ? true : false;
            }
            if (m_SettingDataMgr.GetValriableValue("STYLELINE_WIDTH", ref strValue))
            {
                m_StyleLineWidth = Convert.ToUInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("STYLELINE_COLOR", ref strValue))
            {
                //m_StyleLineColor = Color.FromName(strValue );
            }
            if (m_SettingDataMgr.GetValriableValue("STYLELINE_STYLE", ref strValue))
            {
                m_StyleLineStyle = Convert.ToUInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("JUDGEMENT_DETAIL", ref strValue))
            {
                m_bDetail_info = strValue.Equals("1") ? true : false;
            }
            if (m_SettingDataMgr.GetValriableValue("MARKER_SIZE", ref strValue))
            {
                m_MarkerSize = Convert.ToInt32(strValue);
            }
            if (m_SettingDataMgr.GetValriableValue("FIXED_COMMENT", ref strValue))
            {
                m_FixedComment = strValue;
            }
            if (m_SettingDataMgr.GetValriableValue("HIP_MKO", ref strValue))
            {
                dValue = Convert.ToDouble(strValue);
                m_CalcPostureProp.SetHipMko(dValue);
            }
            if (m_SettingDataMgr.GetValriableValue("BELT_2_KNEE_RATIO", ref strValue))
            {
                dValue = Convert.ToDouble(strValue);
                m_CalcPostureProp.SetBelt2KneeRatio(dValue);
            }
            if (m_SettingDataMgr.GetValriableValue("BELT_2_HIP_RATIO", ref strValue))
            {
                dValue = Convert.ToDouble(strValue);
                m_CalcPostureProp.SetBelt2HipRatio(dValue);
            }
            if (m_SettingDataMgr.GetValriableValue("KNEE_RATIO_ERROR", ref strValue))
            {
                dValue = Convert.ToDouble(strValue);
                m_CalcPostureProp.SetKneeRatioError(dValue);
            }
            if (m_SettingDataMgr.GetValriableValue("SHOULDER_OFFSET0", ref strValue))
            {
                iValue = Convert.ToInt32(strValue);
                m_CalcPostureProp.SetShoulderOffset(0, iValue);
            }
            if (m_SettingDataMgr.GetValriableValue("SHOULDER_OFFSET1", ref strValue))
            {
                iValue = Convert.ToInt32(strValue);
                m_CalcPostureProp.SetShoulderOffset(1, iValue);
            }
            if (m_SettingDataMgr.GetValriableValue("SHOULDER_OFFSET2", ref strValue))
            {
                iValue = Convert.ToInt32(strValue);
                m_CalcPostureProp.SetShoulderOffset(2, iValue);
            }
            if (m_SettingDataMgr.GetValriableValue("IMPORT_YGAFILE", ref strValue))
            {
                iValue = Convert.ToInt32(strValue);
                SetImportYGAFileMenuFlag(iValue);

            }
            if (m_SettingDataMgr.GetValriableValue("LANGUAGE_MENU", ref strValue))
            {
                iValue = Convert.ToInt32(strValue);
                SetLanguageMenu(iValue);

            }
            return true;
        }
        public int GetImportYGAFileMenuFlag()
        {
            return m_ImportYGAFileMenuFlag;
        }
        void SetImportYGAFileMenuFlag(int Value)
        {
            m_ImportYGAFileMenuFlag = Value;
        }
        public int GetLanguageMenu()
        {
            return m_LanguageMenu;
        }
        void SetLanguageMenu(int Value)
        {
            m_LanguageMenu = Value;
        }
        public int CalcPosturePatternID()
        {

            ResultData result = new ResultData();

            CalcBodyBalanceResultData(ref result);
            return result.GetPosturePatternID();
        }
        public void CalcBodyBalanceResultData(ref ResultData result)
        {
            m_BalanceLabData.CalcBodyBalanceResultData(ref result, m_CalibInf.GetCameraAngle(), ref m_ResultActionScriptMgr, ref m_ResultActionScriptMgrSide, GetJudgeMode());
        }

        public int CalcYugamiPoint()//FrontBodyAngle FrontBodyAngleStanding,FrontBodyAngle FrontBodyAngleKneedown,SideBodyAngle angleSide) 
        {

            ResultData result = new ResultData();
            CalcBodyBalanceResultData(ref result);
            return result.CalcYugamiPoint();//FrontBodyAngleStanding,FrontBodyAngleKneedown,angleSide);
        }

        public int CalcYugamiPointRank()//FrontBodyAngle FrontBodyAngleStanding, FrontBodyAngle FrontBodyAngleKneedown, SideBodyAngle angleSide)
        {
            ResultData result = new ResultData();

            CalcBodyBalanceResultData(ref result);
            return result.CalcYugamiPointRank();//FrontBodyAngleStanding, FrontBodyAngleKneedown, angleSide);
        }
        public int CalcNewTrainingIDs()
        {
            int i = 0;
            for (i = 0; i < Constants.DISPLAYED_NEWTRAINING_COUNT_MAX; i++)
            {
                m_aiNewTrainingIDs[i] = Constants.TRAININGID_NONE;
            }

            ResultData result = new ResultData();
            CalcBodyBalanceResultData(ref result);

            int[] aiTrainingID = new int[Constants.TRAININGID_MAX];
            int[] aiPriority = new int[Constants.TRAININGID_MAX];
            int iTrainingCount = 0;

            for (i = 0; i < 92 / 4; i++)
            {
                int iPriority = 0;
                if (s_aTrainingSelectConditionArray[i].Check(ref iPriority, result) == 1)
                {
                    aiTrainingID[iTrainingCount] = i;
                    aiPriority[iTrainingCount] = iPriority;
                    iTrainingCount++;
                }
            }
            // ƒ\[ƒg.
            for (i = 0; i < iTrainingCount; i++)
            {
                int j = 0;
                for (j = i + 1; j < iTrainingCount; j++)
                {
                    if ((aiPriority[j] > aiPriority[i]) ||
                        ((aiPriority[j] == aiPriority[i]) && (aiTrainingID[j] < aiTrainingID[i])))
                    {
                        int iTmp = aiPriority[j];
                        aiPriority[j] = aiPriority[i];
                        aiPriority[i] = iTmp;
                        iTmp = aiTrainingID[j];
                        aiTrainingID[j] = aiTrainingID[i];
                        aiTrainingID[i] = iTmp;
                    }
                }
            }
            if (iTrainingCount <= 0)
            {
                return 0;
            }
            // Å‘å—Dæ‡ˆÊ‚ÆÅ¬—Dæ‡ˆÊ‚ð‹‚ß‚é.
            int iMaxPriority = aiPriority[0];
            int iMinPriority = aiPriority[0];
            for (i = 1; i < iTrainingCount; i++)
            {
                if (aiPriority[i] > iMaxPriority)
                {
                    iMaxPriority = aiPriority[i];
                }
                if (aiPriority[i] < iMinPriority)
                {
                    iMinPriority = aiPriority[i];
                }
            }
            // —Dæ‡ˆÊ‚²‚Æ‚ÉƒVƒƒƒbƒtƒ‹‚ðs‚¤.
            for (i = iMaxPriority; i >= iMinPriority; i--)
            {
                // ‚Ü‚¸A‚»‚Ì—Dæ‡ˆÊ‚ÌƒgƒŒ[ƒjƒ“ƒO‚ª‘¶Ý‚·‚é”ÍˆÍ‚ð‹‚ß‚é.
                int iRangeStart = -1;
                int iRangeEnd = -1;
                int j = 0;
                for (j = 0; j < iTrainingCount; j++)
                {
                    if (aiPriority[j] == i)
                    {
                        if (iRangeStart < 0)
                        {
                            iRangeStart = j;
                            iRangeEnd = j;
                        }
                        else
                        {
                            iRangeEnd = j;
                        }
                    }
                }
                if (iRangeStart >= 0)
                {
                    // ‚»‚Ì—Dæ‡ˆÊ‚ÌƒgƒŒ[ƒjƒ“ƒO‚ª‘¶Ý‚µ‚½
                    // ‹‚Ü‚Á‚½”ÍˆÍ‚Ì’†‚ÅƒVƒƒƒbƒtƒ‹‚ðs‚¤.
                    Random rnd = new Random();
                    for (j = iRangeStart; j < iRangeEnd; j++)
                    {
                        int iTargetIndex = j + (rnd.Next(iRangeEnd - j + 1));
                        if (iTargetIndex != j)
                        {
                            int iTmp = aiPriority[j];
                            aiPriority[j] = aiPriority[iTargetIndex];
                            aiPriority[iTargetIndex] = iTmp;
                            iTmp = aiTrainingID[j];
                            aiTrainingID[j] = aiTrainingID[iTargetIndex];
                            aiTrainingID[iTargetIndex] = iTmp;
                        }
                    }
                }
            }
            if (iTrainingCount > 4)
            {
                iTrainingCount = 4;
            }
            for (i = 0; i < iTrainingCount; i++)
            {
                m_aiNewTrainingIDs[i] = aiTrainingID[i];
            }
            return iTrainingCount;
        }
        public int GetNewTrainingIDs(ref int[] piNewTrianingIDs)
        {
            int iRet = 0;
            int i = 0;
            for (i = 0; i < Constants.DISPLAYED_NEWTRAINING_COUNT_MAX; i++)
            {
                piNewTrianingIDs[i] = m_aiNewTrainingIDs[i];
                if ((m_aiNewTrainingIDs[i] != Constants.TRAININGID_NONE) &&
                    (0 <= m_aiNewTrainingIDs[i]) &&
                    (m_aiNewTrainingIDs[i] < Constants.TRAININGID_MAX))
                {
                    iRet++;
                }
            }
            return iRet;
        }

        public bool LoadSetup5File()
        {
            if (!File.Exists(Constants.programdata_path + "displayconfig.txt"))
            {
                if (!m_SettingDataMgr.ReadFromFile(@"Resources\", "displayconfig.txt"))
                {
                    return false;
                }
                File.Copy(@"Resources\displayconfig.txt",
                       Constants.programdata_path + "displayconfig.txt");
            }
            else if (!m_SettingDataMgr.ReadFromFile(Constants.programdata_path, "displayconfig.txt"))
            {
                return false;
            }
            string strValue = string.Empty;
            if (m_SettingDataMgr.GetValriableValue("MARKERSIZETYPE", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iMarkerSizeType = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYARROW", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayArrow = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYCENTERLINE", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayCenterLine = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYLABEL", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayLabel = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYSCORE", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayScore = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYMUSCLEREPORT", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayMuscleReport = iValue;
            }
            if (m_SettingDataMgr.GetValriableValue("DISPLAYCENTROID", ref strValue))
            {
                int iValue = Convert.ToInt32(strValue);
                m_iDisplayCentroid = iValue;
            }
            return true;
        }
        public void SetMeasurementDlg(IDD_MeasurementDlg IDD)
        {
            m_MeasurementDlg = IDD;
        }
        public IDD_MeasurementDlg GetMeasurementDlg()
        {
            return m_MeasurementDlg;
        }
        public void SetMeasurementStart(IDD_MEASUREMENT_START_VIEW IDD)
        {
            m_MeasurementStart = IDD;
        }
        public IDD_MEASUREMENT_START_VIEW GetMeasurementStart()
        {
            return m_MeasurementStart;
        }
        public void SetJointEditView(JointEditView IDD)
        {
            m_JointEditView = IDD;
        }
        public JointEditView GetJointEditView()
        {
            return m_JointEditView;
        }
        public void SetInitialScreen(IDD_BALANCELABO IDD)
        {
            m_Balancelabo = IDD;
        }
        public IDD_BALANCELABO GetInitialScreen()
        {
            return m_Balancelabo;
        }
        public void SetSettingView(SettingView IDD)
        {
            m_SettingView = IDD;
        }
        public SettingView GetSettingView()
        {
            return m_SettingView;
        }
        public void SetOpenResult(OpenResult IDD)
        {
            m_OpenResult = IDD;
        }
        public OpenResult GetOpenResult()
        {
            return m_OpenResult;
        }

        public void SetSideJointEditView(SideJointEditView IDD)
        {
            m_SideJointEditView = IDD;
        }
        public SideJointEditView GetSideJointEditView()
        {
            return m_SideJointEditView;
        }
        public void SetMainScreen(IDD_BALANCELABO_DIALOG IDD)
        {
            m_Idd_Balancelabo_Dialog = IDD;
        }
        public IDD_BALANCELABO_DIALOG GetMainScreen()
        {
            return m_Idd_Balancelabo_Dialog;
        }
        public void LoadSetup4File() // for judgement mode
        {
            SettingDataMgr SettingDataMgr = new SettingDataMgr();
            if (SettingDataMgr.ReadFromFile(@"Resources\", "judgeconfig.txt"))
            {
                string strValue = string.Empty;
                if (SettingDataMgr.GetValriableValue("JUDGEMODE", ref strValue))
                {
                    m_iJudgeMode = Convert.ToInt32(strValue);
                }

            }
        }
        public bool SaveSetup5File()
        {
            string strValue;
            strValue = m_iMarkerSizeType.ToString();
            if (!m_SettingDataMgr.AddVariableValue("MARKERSIZETYPE", strValue))
            {
                return false;
            }
            strValue = m_iDisplayArrow.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYARROW", strValue))
            {
                return false;
            }
            strValue = m_iDisplayCenterLine.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYCENTERLINE", strValue))
            {
                return false;
            }
            strValue = m_iDisplayLabel.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYLABEL", strValue))
            {
                return false;
            }
            strValue = m_iDisplayScore.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYSCORE", strValue))
            {
                return false;
            }
            strValue = m_iDisplayMuscleReport.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYMUSCLEREPORT", strValue))
            {
                return false;
            }
            strValue = m_iDisplayCentroid.ToString();
            if (!m_SettingDataMgr.AddVariableValue("DISPLAYCENTROID", strValue))
            {
                return false;
            }
            string strModulePath = string.Empty;
            GetModuleFilePath(strModulePath);
            if (!m_SettingDataMgr.WriteToFile(Constants.programdata_path, "displayconfig.txt"))
            {
                return false;
            }
            return true;
        }
        public byte[] Compress(byte[] bytes, bool SwapFlag)
        {
            return m_BalanceLabData.Compress(bytes, SwapFlag);
        }
        public void SetUpdateFlag(int UpdateFlag)
        {
            mUpdateFlag = UpdateFlag;
        }
        public int GetUpdateFlag()
        {
            return mUpdateFlag;
        }
        public void SetUniqueId(int UniqueId)
        {
            m_UniqueId = UniqueId;
        }

        public int GetUniqueId()
        {
            return m_UniqueId;
        }

    }

}




