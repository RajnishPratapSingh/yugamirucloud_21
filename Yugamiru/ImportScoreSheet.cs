﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    class ImportScoreSheet
    {
        public void ImportScoresheetImagesintoDB()
        {
            CDatabase DB = new CDatabase();
            String Query = string.Empty;

            // for English -----------------------------------------
            
            string ScoreSheet_Path = Constants.path + "English";
            String[] filenames = System.IO.Directory.GetFiles(ScoreSheet_Path);

            byte[] data = null;
            
            for (int i = 0; i < filenames.Length; i++)
            {
                try
                {
                    data = File.ReadAllBytes(filenames[i]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                Query = "INSERT INTO ScoreSheetImages_English(ImageFileName,ImageBlob) VALUES ('" +
                    Path.GetFileName(filenames[i]) +
                    "',@img)";
                DB.InsertImage(Query, data);
            }
            
                //For Japanese------------------------------------------------------

                ScoreSheet_Path = Constants.path + "Japanese";
                filenames = System.IO.Directory.GetFiles(ScoreSheet_Path);

                data = null;
            for (int i = 0; i < filenames.Length; i++)
            {
                try
                {
                    data = File.ReadAllBytes(filenames[i]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                Query = "INSERT INTO ScoreSheetImages_Japanese(ImageFileName,ImageBlob) VALUES ('" +
                    Path.GetFileName(filenames[i]) +
                    "',@img)";
                DB.InsertImage(Query, data);
            }
            /*
                    //For Chinese------------------------------------------------------

                    ScoreSheet_Path = Constants.path + "Chinese";
                    filenames = System.IO.Directory.GetFiles(ScoreSheet_Path);

                    data = null;
            for (int i = 0; i < filenames.Length; i++)
            {
                try
                {
                    data = File.ReadAllBytes(filenames[i]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                Query = "INSERT INTO ScoreSheetImages_Chinese(ImageFileName,ImageBlob) VALUES ('" +
                    Path.GetFileName(filenames[i]) +
                    "',@img)";
                DB.InsertImage(Query, data);
            }

                        //For Korean------------------------------------------------------

                        ScoreSheet_Path = Constants.path + "Korean";
                        filenames = System.IO.Directory.GetFiles(ScoreSheet_Path);

                        data = null;
                        for (int i = 0; i < filenames.Length; i++)
                        {
                            try
                            {
                                data = File.ReadAllBytes(filenames[i]);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }


                            Query = "INSERT INTO ScoreSheetImages_Korean(ImageFileName,ImageBlob) VALUES ('" +
                                Path.GetFileName(filenames[i]) +
                                "',@img)";
                            DB.InsertImage(Query, data);

                        }

            //For Thai------------------------------------------------------

            ScoreSheet_Path = Constants.path + "Thai";
            filenames = System.IO.Directory.GetFiles(ScoreSheet_Path);

            data = null;
            for (int i = 0; i < filenames.Length; i++)
            {
                try
                {
                    data = File.ReadAllBytes(filenames[i]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                Query = "INSERT INTO ScoreSheetImages_Thai(ImageFileName,ImageBlob) VALUES ('" +
                    Path.GetFileName(filenames[i]) +
                    "',@img)";
                DB.InsertImage(Query, data);

            }
            */
        }
    }
}
