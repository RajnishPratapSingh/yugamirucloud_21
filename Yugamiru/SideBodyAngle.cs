﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class SideBodyAngle
    {
        double m_dBodyBalance = 0 ;
        double m_dNeckForwardLeaning = 0;
        double m_dShoulderEarAngle = 0;
        double m_dPelvicAngle = 0;
        public SideBodyAngle()
        {
        
        }
        ~SideBodyAngle()
        {
        }


   public void Clear()
    {
        m_dBodyBalance = -999;
        m_dNeckForwardLeaning = -999;
        m_dShoulderEarAngle = -999;
        m_dPelvicAngle = -999;
    }

   public void CalcByPosition( ref SideBodyPosition pBodyPos, double dCameraAngle)
    {
        // ŠÖßˆÊ’u‚©‚çŠp“x‚ðŽZo‚µAƒJƒƒ‰‚ÌŒX‚«•ª·‚µˆø‚­
        int[] midloins = new int[3]; // œ”Õ‚Ì’†Si‘¤–Êj
        midloins[2] = pBodyPos.GetHipX();
        midloins[1] = pBodyPos.GetHipY();

        int[] midshoulder = new int[3]; // —¼Œ¨‚Ì’†S
        midshoulder[2] = pBodyPos.GetShoulderX();
        midshoulder[1] = pBodyPos.GetShoulderY();

        // ˜-Œ¨’†S‚ÌŒX‚«
        m_dBodyBalance = Math.Atan2((double)(midshoulder[2] - midloins[2]),
                         (double)-(midshoulder[1] - midloins[1]))
                         * 180.0 / Math.PI - dCameraAngle;

        int[] midear = new int[3]; //—¼Ž¨‚Ì’†S.
        midear[2] = pBodyPos.GetEarX();
        midear[1] = pBodyPos.GetEarY();

        double angle1 = (Math.Atan2((double)(midshoulder[2] - midloins[2]),
                (double)-(midshoulder[1] - midloins[1])) * 180.0 / Math.PI - dCameraAngle);

        double angle2 = (Math.Atan2((double)(midear[2] - midshoulder[2]),
                (double)-(midear[1] - midshoulder[1])) * 180.0 / Math.PI - dCameraAngle);

        double angle3 = angle1 - angle2;
        if (angle3 > 360)
        {
            angle3 = angle3 - 360;
        }
        else if (angle3 < -360)
        {
            angle3 = -((-angle3) - 360);
        }
        if (angle3 > 180)
        {
            angle3 -= 360;
        }
        if (angle3 < -180)
        {
            angle3 += 360;
        }
        m_dNeckForwardLeaning = angle3;

        m_dShoulderEarAngle = (Math.Atan2((double)(pBodyPos.GetEarX() - pBodyPos.GetShoulderX()),
                                            (double)-(pBodyPos.GetEarY() - pBodyPos.GetShoulderY())) * 180.0 / 3.141592
                                            - dCameraAngle);
        m_dPelvicAngle = (Math.Atan2((double)(pBodyPos.GetLeftBeltY() - pBodyPos.GetRightBeltY()),
                                            (double)(pBodyPos.GetLeftBeltX() - pBodyPos.GetRightBeltX())) * 180 / 3.141592
                                            - dCameraAngle);
    }

  public   double GetBodyBalance( ) 
{
	return m_dBodyBalance;
}

public double GetNeckForwardLeaning( ) 
{
	return m_dNeckForwardLeaning;
}

        public double GetShoulderEarAngle() 
{
	return m_dShoulderEarAngle;
}

        public double GetPelvicAngle() 
{
	return m_dPelvicAngle;
}

    }
}
