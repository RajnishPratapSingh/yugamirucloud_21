﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Windows;
using System.Windows.Input;
using static Yugamiru.stretchDIBbits;
using System.Windows.Forms;

namespace Yugamiru
{
    public class ImageClipWnd
    {
        public byte[] m_pbyteBits;
        public byte[] m_resizedBits;
        public stretchDIBbits.BITMAPINFO m_bmi = new stretchDIBbits.BITMAPINFO();
        public Graphics m_pDCOffscreen;
        public Bitmap m_pbmOffscreen;
        public Bitmap m_pbmOffscreenOld;
        public int m_iOffscreenWidth;
        public int m_iOffscreenHeight;
        public int m_iBackgroundWidth;
        public int m_iBackgroundHeight;
        public int m_iDestRectWidth;
        public int m_iDestRectHeight;
        public int m_iDestRectUpperLeftCornerX;
        public int m_iDestRectUpperLeftCornerY;
        public int m_iSelectionFrameWidth;
        public int m_iSelectionFrameHeight;
        public int m_iSelectionFrameUpperLeftCornerX;
        public int m_iSelectionFrameUpperLeftCornerY;
        public int m_iMouseCaptureMode;
        public bool m_bDragAcceptFilesFlag;
        public string m_strDragFileName;
        public Rectangle m_SourceRect;
        public Rectangle m_DestRect;
        
        public ImageClipWnd()
        {

            m_pbyteBits = null;
            m_resizedBits = null;


            m_pbmOffscreen = null;

            m_pbmOffscreenOld = null;

            m_iOffscreenWidth = 0;

            m_iOffscreenHeight = 0;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            m_iDestRectWidth = 0;

            m_iDestRectHeight = 0;

            m_iDestRectUpperLeftCornerX = 0;

            m_iDestRectUpperLeftCornerY = 0;

            m_iSelectionFrameWidth = 0;

            m_iSelectionFrameHeight = 0;

            m_iSelectionFrameUpperLeftCornerX = 0;

            m_iSelectionFrameUpperLeftCornerY = 0;

            m_iMouseCaptureMode = 0;

            m_bDragAcceptFilesFlag = false;

            //memset(&m_bmi, 0, sizeof(m_bmi));
        }

        ~ImageClipWnd()
        {
            //Cleanup();
        }

        public void Cleanup()
        {
            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            if (m_pDCOffscreen != null)
            {
                if (m_pbmOffscreenOld != null)
                {
                    //m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
                //delete m_pDCOffscreen;
                m_pDCOffscreen = null;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
        }




        // CImageClipWnd ƒƒbƒZ[ƒW ƒnƒ“ƒhƒ‰



        public void OnPaint()
        {
            //CPaintDC dc(this); // device context for painting
            //Graphics dc = this.CreateGraphics();

            Rectangle rcClient = new Rectangle();
            //GetClientRect(rcClient);
            if ((m_pDCOffscreen != null) &&
                (rcClient.Width == m_iOffscreenWidth) &&
                (rcClient.Height == m_iOffscreenHeight))
            {
                /* dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), m_pDCOffscreen, 0, 0, SRCCOPY);*/

            }
            else
            {
                //  dc.FillSolidRect(&rcClient, RGB(0, 0, 0));
            }
        }

        public void OnDestroy()
        {
            Cleanup();
            //CWnd::OnDestroy();
        }

        void OnSize(uint nType, int cx, int cy)
        {
            //CWnd::OnSize(nType, cx, cy);
            if (m_pDCOffscreen != null)
            {
                if ((cx == m_iOffscreenWidth) && (cy == m_iOffscreenHeight))
                {
                    // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ÌÄ¶¬•s—v.
                    return;
                }
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðÄ¶¬.
                if (m_pbmOffscreenOld != null)
                {
                    // m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
            }
            else
            {
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðV‹K¶¬.
                /* m_pDCOffscreen = new CDC;
                 CDC* pDC = GetDC();
                 m_pDCOffscreen->CreateCompatibleDC(pDC);
                 ReleaseDC(pDC);
                 pDC = NULL;*/
            }

            if (m_pbmOffscreen == null)
            {
                // m_pbmOffscreen = new CBitmap;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
            /*        m_pbmOffscreen = new CBitmap;
                    CDC* pDC = GetDC();
                    m_pbmOffscreen->CreateCompatibleBitmap(pDC, cx, cy);
                    ReleaseDC(pDC);
                    pDC = NULL;
                    m_pbmOffscreenOld = m_pDCOffscreen->SelectObject(m_pbmOffscreen);
                    if (m_pbyteBits != NULL)
                    {
                        m_pDCOffscreen->SetStretchBltMode(HALFTONE);

                ::StretchDIBits(m_pDCOffscreen->GetSafeHdc(), 0, 0, cx, cy,
                    0, (m_iBackgroundHeight - 1 - (0 + m_iBackgroundHeight - 1)),
                    m_iBackgroundWidth, m_iBackgroundHeight,
                    m_pbyteBits, &m_bmi, DIB_RGB_COLORS, SRCCOPY);
                    }
                    else
                    {
                        m_pDCOffscreen->FillSolidRect(0, 0, cx, cy, RGB(0, 0, 0));
                    }
                    m_iOffscreenWidth = cx;
                    m_iOffscreenHeight = cy;*/
        }

        bool OnEraseBkgnd(Graphics pDC)
        {
            return true;
        }

        public Image<Bgr,Byte> SetBackgroundBitmap(Bitmap pchFileName)
        {

            //IplImage* piplimageSrc = ::cvLoadImage(pchFileName, CV_LOAD_IMAGE_COLOR);
            
           
             Image<Bgr,Byte>piplimageSrc = new Image<Bgr, Byte>(pchFileName);

          /*  if (piplimageSrc == null)
            {
                return false;
            }*/
            bool bRet = SetBackgroundBitmap(piplimageSrc);
            /*if (piplimageSrc != null)
            {
                //::cvReleaseImage( &piplimageSrc );
                piplimageSrc.Dispose();
                piplimageSrc = null;
            }*/
            
            return piplimageSrc;
        }
        /*
        public bool SetBackgroundBitmap( const ITEMIDLIST* pItemIDListImageFile)
        {
            DWORD dwTick1 = GetTickCount();

            IShellFolder* pShellFolderDesktop = NULL;
            if (SHGetDesktopFolder(&pShellFolderDesktop) != S_OK)
            {
                return FALSE;
            }
            IStream* pStream = NULL;
            HRESULT hRes = pShellFolderDesktop->BindToObject(pItemIDListImageFile, NULL, IID_IStream, (void**)&pStream);
            if (hRes != S_OK)
            {
                if (pShellFolderDesktop != NULL)
                {
                    pShellFolderDesktop->Release();
                    pShellFolderDesktop = NULL;
                }
                return FALSE;
            }
            if (pShellFolderDesktop != NULL)
            {
                pShellFolderDesktop->Release();
                pShellFolderDesktop = NULL;
            }
            LARGE_INTEGER li;
            li.HighPart = 0;
            li.LowPart = 0;
            // Œø—¦‰»‚Ì‚½‚ßAƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚Ö‚ÌƒRƒs[‚ª‰Â”\‚È‚ç‚ÎAƒRƒs[‚ðs‚¤B.
            IStream* pStreamMem = NULL;
            if (CreateStreamOnHGlobal(NULL, TRUE, &pStreamMem) == S_OK)
            {
                // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€¶¬¬Œ÷.
                //OutputDebugString("CreateStreamOnHGlobal Succeeded\n");
                ULARGE_INTEGER uli;
                uli.HighPart = 0xFFFFFFFF;
                uli.LowPart = 0xFFFFFFFF;
                if (pStream->CopyTo(pStreamMem, uli, NULL, NULL) == S_OK)
                {
                    //OutputDebugString("CopyTo Succeeded\n");
                    // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ÉƒRƒs[¬Œ÷.
                    if (pStreamMem->Seek(li, SEEK_SET, NULL) == S_OK)
                    {
                        //OutputDebugString("Seek Succeeded\n");
                        // æ“ª‚ÉƒV[ƒN¬Œ÷.
                        // Œ´ƒXƒgƒŠ[ƒ€‚ð‰ð•ú‚µA¡Œã‚ÍŒ´ƒXƒgƒŠ[ƒ€‚Ì‘ã‚í‚è‚É.
                        // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ðŽg—p‚·‚éB.
                        pStream->Release();
                        pStream = pStreamMem;
                        pStreamMem = NULL;
                    }
                    else
                    {
                        // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ÌƒV[ƒN‚ÉŽ¸”s‚µ‚½.
                        // Œ´ƒXƒgƒŠ[ƒ€‚ÌƒV[ƒN‚ðs‚¤.
                        if (pStream->Seek(li, SEEK_SET, NULL) != S_OK)
                        {
                            if (pStreamMem != NULL)
                            {
                                pStreamMem->Release();
                                pStreamMem = NULL;
                            }
                            return FALSE;
                        }
                    }
                }
                if (pStreamMem != NULL)
                {
                    pStreamMem->Release();
                    pStreamMem = NULL;
                }

            }

            //ULARGE_INTEGER uli;
            //if ( IStream_Size( pStream, &uli ) == S_OK ){
            //	char szMessage[100];
            //	sprintf( &(szMessage[0]), "Stream Size %I64u\n", uli );
            //	OutputDebugString( &(szMessage[0]) );
            //}

            //DWORD dwTick1 = GetTickCount();

            IplImage* piplimageSrc = cvexJPEGDecodeFromStream(pStream, 0);
            if (pStream != NULL)
            {
                pStream->Release();
                pStream = NULL;
            }
        #if 0
            DWORD dwTick2 = GetTickCount();
            {
                char szMessage[100];
                sprintf( &(szMessage[0]), "Tick %d\n", dwTick2-dwTick1 );
                OutputDebugString( &(szMessage[0]) );
            }
        #endif
            if (piplimageSrc == NULL)
            {
                return FALSE;
            }
            BOOL bRet = SetBackgroundBitmap(piplimageSrc);
            if (piplimageSrc != NULL)
            {

                ::cvReleaseImage(&piplimageSrc);
                piplimageSrc = NULL;
            }
            return bRet;
        }
        */
        public bool SetBackgroundBitmap(Image<Bgr, byte> piplimageSrc)
        {
            m_pbyteBits = new byte[piplimageSrc.Width*piplimageSrc.Height* piplimageSrc.MIplImage.NChannels];        
            m_pbyteBits = piplimageSrc.Bytes;

            if (m_pbyteBits == null)
            {
                return false;
            }

            m_bmi.bmiHeader.biSize = 40;//new stretchDIBbits.BITMAPINFOHEADER().biSize;
            m_bmi.bmiHeader.biWidth = piplimageSrc.Width;//1024;
            m_bmi.bmiHeader.biHeight = -piplimageSrc.Height;//-1280;
                m_bmi.bmiHeader.biPlanes		= 1;
                m_bmi.bmiHeader.biBitCount		= 24;
                m_bmi.bmiHeader.biCompression	= 0;
                m_bmi.bmiHeader.biSizeImage		= 0;
                m_bmi.bmiHeader.biXPelsPerMeter	= 0;
                m_bmi.bmiHeader.biYPelsPerMeter	= 0;
                m_bmi.bmiHeader.biClrUsed		= 0;
                m_bmi.bmiHeader.biClrImportant	= 0;

            m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };// Array of Object
            m_bmi.bmiColors[0].rgbBlue		= 255;
                m_bmi.bmiColors[0].rgbGreen		= 255;
                m_bmi.bmiColors[0].rgbRed		= 255;
                m_bmi.bmiColors[0].rgbReserved	= 255;
                

            //m_bmi = new Bitmap(piplimageSrc.Width, piplimageSrc.Height,PixelFormat.Format24bppRgb);
           m_iOffscreenWidth = 384;
           m_iOffscreenHeight = 480;

            m_iBackgroundWidth = /*1024;*/piplimageSrc.Width;
            m_iBackgroundHeight = /*1280;*/piplimageSrc.Height;

            int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
            m_iDestRectWidth = m_iBackgroundWidth * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectHeight = m_iBackgroundHeight * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
            m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

            m_iSelectionFrameWidth = m_iBackgroundHeight * 1024 / 1280;
            m_iSelectionFrameHeight = m_iBackgroundHeight;
            m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
            m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

            return true;
        }

        public bool SetBackgroundBitmap(int iWidth, int iHeight, byte[] pbyteBits)
        {
            m_bmi.bmiHeader.biSize = 40;//new stretchDIBbits.BITMAPINFOHEADER().biSize;//sizeof(BITMAPINFOHEADER);
            m_bmi.bmiHeader.biWidth = iWidth;
              m_bmi.bmiHeader.biHeight = -iHeight;
              m_bmi.bmiHeader.biPlanes = 1;
              m_bmi.bmiHeader.biBitCount = 24;
              m_bmi.bmiHeader.biCompression = 0;
              m_bmi.bmiHeader.biSizeImage = 0;
              m_bmi.bmiHeader.biXPelsPerMeter = 0;
              m_bmi.bmiHeader.biYPelsPerMeter = 0;
              m_bmi.bmiHeader.biClrUsed = 0;
              m_bmi.bmiHeader.biClrImportant = 0;

            m_bmi.bmiColors = new RGBQUAD[]  // Array of Object
           {
                new RGBQUAD
                {

                }
           };
            m_bmi.bmiColors[0].rgbBlue = 255;
              m_bmi.bmiColors[0].rgbGreen = 255;
              m_bmi.bmiColors[0].rgbRed = 255;
              m_bmi.bmiColors[0].rgbReserved = 255;
            // m_bmi.Size = new Size(iWidth, iHeight);

            m_iBackgroundWidth = iWidth;
            m_iBackgroundHeight = iHeight;

            m_iOffscreenWidth = 384;
            m_iOffscreenHeight = 480;
            /*
                        int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
                        int iBmpBitsSize = iBmpWidthStep * iHeight;
                        if (iBmpBitsSize <= 0)
                        {
                            return false;
                        }

                        if (m_pbyteBits != null)
                        {
                            //delete[] m_pbyteBits;
                            m_pbyteBits = null;
                        }
                        m_pbyteBits = new byte[iBmpBitsSize];
                        if (m_pbyteBits == null)
                        {
                            return false;
                        }
                        int i = 0;
                        for (i = 0; i < iBmpBitsSize; i++)
                        {
                            m_pbyteBits[i] = pbyteBits[i];
                        }*/
            m_pbyteBits = pbyteBits;

            int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
            m_iDestRectWidth = m_iBackgroundWidth * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectHeight = m_iBackgroundHeight * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
            m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

            m_iSelectionFrameWidth = m_iBackgroundHeight * 1024 / 1280;
            m_iSelectionFrameHeight = m_iBackgroundHeight;
            m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
            m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

            return true;
        }

        public bool SetBackgroundBitmap(int iWidth, int iHeight)
        {
             m_bmi.bmiHeader.biSize = new stretchDIBbits.BITMAPINFOHEADER().biSize;//sizeof(BITMAPINFOHEADER);
            m_bmi.bmiHeader.biWidth = iWidth;
             m_bmi.bmiHeader.biHeight = iHeight;
             m_bmi.bmiHeader.biPlanes = 1;
             m_bmi.bmiHeader.biBitCount = 24;
             m_bmi.bmiHeader.biCompression = 0;
             m_bmi.bmiHeader.biSizeImage = 0;
             m_bmi.bmiHeader.biXPelsPerMeter = 0;
             m_bmi.bmiHeader.biYPelsPerMeter = 0;
             m_bmi.bmiHeader.biClrUsed = 0;
             m_bmi.bmiHeader.biClrImportant = 0;

            m_bmi.bmiColors = new RGBQUAD[]  // Array of Object
          {
                new RGBQUAD
                {

                }
          };
            m_bmi.bmiColors[0].rgbBlue = 255;
             m_bmi.bmiColors[0].rgbGreen = 255;
             m_bmi.bmiColors[0].rgbRed = 255;
             m_bmi.bmiColors[0].rgbReserved = 255;
           // m_bmi = new Bitmap(iWidth, iHeight, PixelFormat.Format24bppRgb);


            m_iBackgroundWidth = iWidth;
            m_iBackgroundHeight = iHeight;

            m_iOffscreenWidth = 384;
            m_iOffscreenHeight = 480;

            int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * iHeight;
            if (iBmpBitsSize <= 0)
            {
                return false;
            }

            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            m_pbyteBits = new byte[iBmpBitsSize];
            if (m_pbyteBits == null)
            {
                return false;
            }
            int i = 0;
            for (i = 0; i < iBmpBitsSize; i++)
            {
                m_pbyteBits[i] = 0;
            }

            int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
            m_iDestRectWidth = m_iBackgroundWidth * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectHeight = m_iBackgroundHeight * m_iOffscreenWidth / iExtendedBitmapWidth;
            m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
            m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

            m_iSelectionFrameWidth = m_iBackgroundHeight * 1024 / 1280;
            m_iSelectionFrameHeight = m_iBackgroundHeight;
            m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
            m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

            return true;
        }

        public void UpdateOffscreen(Graphics m_pDCOffscreen)
        {
        /*   Image<Bgr, byte> emgucvimage = new Image<Bgr, byte>(Yugamiru.Properties.Resources.sokui);
            byte[] emgucv_bytes = new byte[emgucvimage.Width * emgucvimage.Height * emgucvimage.MIplImage.nChannels];
            emgucv_bytes = emgucvimage.Bytes;*/
            if (m_pDCOffscreen == null)
            {
                return;
            }

            if (m_pbyteBits != null)
            {

                 SolidBrush m_Brush = new SolidBrush(Color.FromArgb(0, 0, 0));

                 m_pDCOffscreen.FillRectangle(m_Brush, 0, 0, m_iOffscreenWidth, m_iOffscreenHeight);
                stretchDIBbits.SetStretchBltMode(m_pDCOffscreen.GetHdc(),stretchDIBbits.StretchBltMode.STRETCH_HALFTONE);

                m_pDCOffscreen.ReleaseHdc();
                stretchDIBbits.StretchDIBits(
                            m_pDCOffscreen.GetHdc(),
                            m_iDestRectUpperLeftCornerX,
                            m_iDestRectUpperLeftCornerY,
                            m_iDestRectWidth,
                            m_iDestRectHeight,
                            0,
                            (m_iBackgroundHeight - 1 - (0 + m_iBackgroundHeight - 1)),
                            m_iBackgroundWidth,//1024,
                m_iBackgroundHeight,//1280,
                            m_pbyteBits,//emgucv_bytes 
                            ref m_bmi,
                            Constants.DIB_RGB_COLORS,
                            Constants.SRCCOPY);
                m_pDCOffscreen.ReleaseHdc();


                Pen penNew = new Pen(Color.FromArgb(255, 0, 0));
                //Pen* ppenOld = m_pDCOffscreen->SelectObject(&penNew);
                int iXPos = m_iSelectionFrameUpperLeftCornerX * m_iDestRectWidth / m_iBackgroundWidth + m_iDestRectUpperLeftCornerX;
                int iYPos = m_iSelectionFrameUpperLeftCornerY * m_iDestRectHeight / m_iBackgroundHeight + m_iDestRectUpperLeftCornerY;
                int iWidth = m_iSelectionFrameWidth * m_iDestRectWidth / m_iBackgroundWidth;
                int iHeight = m_iSelectionFrameHeight * m_iDestRectHeight / m_iBackgroundHeight;
        
                /*
                        m_pDCOffscreen->MoveTo(iXPos, iYPos);
                        m_pDCOffscreen->LineTo(iXPos + iWidth, iYPos);
                        m_pDCOffscreen->LineTo(iXPos + iWidth, iYPos + iHeight);
                        m_pDCOffscreen->LineTo(iXPos, iYPos + iHeight);
                        m_pDCOffscreen->LineTo(iXPos, iYPos);
                        m_pDCOffscreen->SelectObject(ppenOld);*/

                m_pDCOffscreen.DrawRectangle(penNew,iXPos,iYPos,iWidth,iHeight);
               // m_pDCOffscreen.DrawLine(penNew, iXPos, iYPos, iXPos + iWidth, iYPos);

                //m_pDCOffscreen.DrawLine(penNew, iXPos + iWidth, iYPos + iHeight, iXPos, iYPos + iHeight);
                //m_pDCOffscreen->LineTo(iXPos, iYPos + iHeight);
                //m_pDCOffscreen->LineTo(iXPos, iYPos);
                //m_pDCOffscreen->SelectObject(ppenOld);

            }
        }

        public int GetBackgroundWidth()
        {
            return m_iBackgroundWidth;
        }

        public int GetBackgroundHeight()
        {
            return m_iBackgroundHeight;
        }

        public int GetSelectedBitmapWidth()
        {
            return 1024;
            
        }

        public int GetSelectedBitmapHeight()
        {
            return 1280;
            
        }

        /*int OnCreate(LPCREATESTRUCT lpCreateStruct)
        {
            if (CWnd::OnCreate(lpCreateStruct) == -1)
            {
                return -1;
            }
            DragAcceptFiles(m_bDragAcceptFilesFlag);
            return 0;
        }
        */
        public void OnLButtonDown(uint nFlags, Point point)
        {
            //SetCapture();
            // Mouse.Capture(this)
            m_iMouseCaptureMode = 1;
            int iSelectionFrameCenterX = (point.X - m_iDestRectUpperLeftCornerX) * m_iBackgroundWidth / m_iDestRectWidth;
            if (iSelectionFrameCenterX < 0)
            {
                iSelectionFrameCenterX = 0;
            }
            if (iSelectionFrameCenterX >= m_iBackgroundWidth)
            {
                iSelectionFrameCenterX = m_iBackgroundWidth;
            }
            m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX - m_iSelectionFrameWidth / 2;
        }

        public void OnLButtonUp(uint nFlags, Point point)
        {
            //ReleaseCapture();
            m_iMouseCaptureMode = 0;
        }

        public void OnMouseMove(uint nFlags, Point point)
        {
            if (m_iMouseCaptureMode > 0)
            {
                int iSelectionFrameCenterX = (point.X - m_iDestRectUpperLeftCornerX) * m_iBackgroundWidth / m_iDestRectWidth;
                if (iSelectionFrameCenterX < 0)
                {
                    iSelectionFrameCenterX = 0;
                }
                if (iSelectionFrameCenterX >= m_iBackgroundWidth)
                {
                    iSelectionFrameCenterX = m_iBackgroundWidth;
                }
                m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX - m_iSelectionFrameWidth / 2;
               
                //UpdateOffscreen(m_pDCOffscreen);
                Rectangle rcClient;
                /* GetClientRect(rcClient);
                 InvalidateRect(&rcClient);
                 UpdateWindow();*/
            }
        }

        public int CalcBackgroundBitmapSize()
        {
            return ((m_iBackgroundWidth * 3 + 3) / 4 * 4 * m_iBackgroundHeight);
        }

        public int CalcSelectedBitmapSize()
        {
            return ((GetSelectedBitmapWidth() * 3 + 3) / 4 * 4 * GetSelectedBitmapHeight());
        }

        public void GetBackgroundBitmap(Graphics hDCRef, byte[] pbyteBits)
        {
            int iBmpWidthStep = (m_iBackgroundWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * m_iBackgroundHeight;
            if (iBmpBitsSize > 0)
            {
                int i = 0;
                for (i = 0; i < iBmpBitsSize; i++)
                {
                    pbyteBits[i] = m_pbyteBits[i];
                }
            }
        }

        public void GetSelectedBitmap(Graphics hDCRef, /*Bitmap image1)*/  ref byte[] pbyteBits)
        {
            
            //Bitmap bmi;
            stretchDIBbits.BITMAPINFO bmi = new stretchDIBbits.BITMAPINFO();
            bmi.bmiHeader.biSize = 40; //new stretchDIBbits.BITMAPINFOHEADER().biSize;
            bmi.bmiHeader.biWidth = 1024;//GetSelectedBitmapWidth();
            bmi.bmiHeader.biHeight = -1280; //GetSelectedBitmapHeight();
            bmi.bmiHeader.biPlanes = 1;
            bmi.bmiHeader.biBitCount = 24;
            bmi.bmiHeader.biCompression = 0;
            bmi.bmiHeader.biSizeImage = 0;
            bmi.bmiHeader.biXPelsPerMeter = 0;
            bmi.bmiHeader.biYPelsPerMeter = 0;
            bmi.bmiHeader.biClrUsed = 0;
            bmi.bmiHeader.biClrImportant = 0;
            bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            bmi.bmiColors[0].rgbBlue = 255;
            bmi.bmiColors[0].rgbGreen = 255;
            bmi.bmiColors[0].rgbRed = 255;
            bmi.bmiColors[0].rgbReserved = 255;
            //bmi = new Bitmap(GetSelectedBitmapWidth(), GetSelectedBitmapHeight(), PixelFormat.Format24bppRgb);

            //IntPtr pDCRef = hDCRef.GetHdc();
             IntPtr dcMem;

            
                        dcMem = CreateCompatibleDC(hDCRef.GetHdc());
                        hDCRef.ReleaseHdc();
                        IntPtr bmMem;
                        //bmMem = stretchDIBbits.CreateCompatibleBitmap(hDCRef.GetHdc(), GetSelectedBitmapWidth(), GetSelectedBitmapHeight());
                        bmMem = stretchDIBbits.CreateCompatibleBitmap(hDCRef.GetHdc(), 384, 480);
                        hDCRef.ReleaseHdc();
                        stretchDIBbits.SelectObject(dcMem, bmMem);
                        //hDCRef.ReleaseHdc();
               


            SolidBrush m_Brush = new SolidBrush(Color.FromArgb(0, 0, 0));
             stretchDIBbits.SetStretchBltMode(hDCRef.GetHdc(), StretchBltMode.STRETCH_HALFTONE);
                hDCRef.ReleaseHdc();
            int iDstLeft = 0;
            int iDstTop = 0;
            int iDstRight = iDstLeft + 384;//GetSelectedBitmapWidth();
            int iDstBottom = iDstTop + 480;//GetSelectedBitmapHeight();
            int iSrcLeft = m_iSelectionFrameUpperLeftCornerX;
            int iSrcTop = m_iSelectionFrameUpperLeftCornerY;
            int iSrcRight = iSrcLeft + m_iSelectionFrameWidth;
            int iSrcBottom = iSrcTop + m_iSelectionFrameHeight;
            int iSrcLeft2 = iSrcLeft;
            int iDstLeft2 = iDstLeft;
            if (iSrcLeft2 < 0)
            {
                iSrcLeft2 = 0;
                iDstLeft2 = iDstLeft + (iSrcLeft2 - iSrcLeft) * (iDstRight - iDstLeft) / (iSrcRight - iSrcLeft);
            }
            int iSrcRight2 = iSrcRight;
            int iDstRight2 = iDstRight;
            if (iSrcRight2 > m_bmi.bmiHeader.biWidth)
            {
                iSrcRight2 = m_bmi.bmiHeader.biWidth;
                iDstRight2 = iDstRight + (iSrcRight2 - iSrcRight) * (iDstRight - iDstLeft) / (iSrcRight - iSrcLeft);
            }
            int iSrcTop2 = iSrcTop;
            int iDstTop2 = iDstTop;
            if (iSrcTop2 < 0)
            {
                iSrcTop2 = 0;
                iDstTop2 = iDstTop + (iSrcTop2 - iSrcTop) * (iDstBottom - iDstTop) / (iSrcBottom - iSrcTop);
            }
            int iSrcBottom2 = iSrcBottom;
            int iDstBottom2 = iDstBottom;
            if (iSrcBottom2 > -(m_bmi.bmiHeader.biHeight))
            {
                iSrcBottom2 = m_bmi.bmiHeader.biHeight;
                iDstBottom2 = iDstBottom + (iSrcBottom2 - iSrcBottom) * (iDstBottom - iDstTop) / (iSrcBottom - iSrcTop);
            }

            if ((iSrcRight2 > iSrcLeft2) && (iSrcBottom2 > iSrcTop2))
            {

                m_DestRect = new Rectangle(
                    iDstLeft2,
                    iDstTop2,
                    iDstRight2 - iDstLeft2,
                    iDstBottom2 - iDstTop2);
                m_SourceRect = new Rectangle(
                    iSrcLeft2,
                    (m_iBackgroundHeight - 1 - (iSrcBottom2 - 1)),
                    iSrcRight2 - iSrcLeft2,
                    iSrcBottom2 - iSrcTop2);
               
               
                stretchDIBbits.StretchDIBits(hDCRef.GetHdc(),
                    iDstLeft2, iDstTop2, iDstRight2 - iDstLeft2, iDstBottom2 - iDstTop2,
                    iSrcLeft2, (m_iBackgroundHeight - 1 - (iSrcBottom2 - 1)),
                    iSrcRight2 - iSrcLeft2, iSrcBottom2 - iSrcTop2,
                    m_pbyteBits, ref m_bmi, Constants.DIB_RGB_COLORS, Constants.SRCCOPY);
             
                hDCRef.ReleaseHdc();
            }
            //::GetDIBits(hDCRef, (HBITMAP)(bmMem.GetSafeHandle()), 0, GetSelectedBitmapHeight(), pbyteBits, &bmi, DIB_RGB_COLORS );
            /*int numRead = stretchDIBbits.GetDIBits(hDCRef.GetHdc(),bmMem,0,(uint)GetSelectedBitmapHeight(),pbyteBits,ref bmi,
                stretchDIBbits.DIB_Color_Mode.DIB_RGB_COLORS);*/
         /*   stretchDIBbits.SelectObject(dcMem, bmMem);
            pbyteBits = new byte[384 * 480 * 3];
            int numRead = stretchDIBbits.GetDIBits(hDCRef.GetHdc(), bmMem, 0, 480,
                pbyteBits, ref bmi, stretchDIBbits.DIB_Color_Mode.DIB_RGB_COLORS);
            hDCRef.ReleaseHdc();*/
/*
            hDCRef.CopyFromScreen(new Point(0, 0),
        new Point(384, 480), new Size(100, 100));*/

            //dcMem.ReleaseHdc();
        }

        public void RotateClockwiseBackgroundBitmap(Graphics hDCRef)
        {
            int iSrcImageWidth = m_iBackgroundWidth;
            int iSrcImageHeight = m_iBackgroundHeight;
            int iSrcImageWidthStep = (iSrcImageWidth * 3 + 3) / 4 * 4;
            int iSrcImageAllocSize = iSrcImageWidthStep * iSrcImageHeight;
            if (iSrcImageAllocSize <= 0)
            {
                return;
            }
            int iDestImageWidth = m_iBackgroundHeight;
            int iDestImageHeight = m_iBackgroundWidth;
            int iDestImageWidthStep = (iDestImageWidth * 3 + 3) / 4 * 4;
            int iDestImageAllocSize = iDestImageWidthStep * iDestImageHeight;
            if (iDestImageAllocSize <= 0)
            {
                return;
            }

            
            /*
            unsigned char* pbyteBitsSrc = new unsigned char[iSrcImageAllocSize];
            unsigned char* pbyteBitsDest = new unsigned char[iDestImageAllocSize];*/
            
                        byte[] pbyteBitsSrc = new byte[iSrcImageAllocSize];
                        byte[] pbyteBitsDest = new byte[iDestImageAllocSize];

                        if ((pbyteBitsSrc == null) || (pbyteBitsDest == null))
                        {
                            if (pbyteBitsSrc != null)
                            {
                                //delete[] pbyteBitsSrc;
                                pbyteBitsSrc = null;
                            }
                            if (pbyteBitsDest != null)
                            {
                                //delete[] pbyteBitsDest;
                                pbyteBitsDest = null;
                            }
                        }
                        GetBackgroundBitmap(hDCRef, pbyteBitsSrc);
                        int i = 0;
                        int j = 0;
                        for (i = 0; i < iDestImageHeight; i++)
                        {
                            //const unsigned char* pbyteSrcBase = pbyteBitsSrc + 3 * i;
                            byte[] pbyteSrcBase = new byte[pbyteBitsSrc.Length + 3 * i];//
                            //unsigned char* pbyteDestBase = pbyteBitsDest + (iDestImageHeight - 1 - i) * iDestImageWidthStep;
                            byte[] pbyteDestBase = new byte[pbyteBitsDest.Length + (iDestImageHeight - 1 - i) * iDestImageWidthStep];
                            for (j = 0; j < iDestImageWidth; j++)
                            {
                                //const unsigned char* pbyteSrc = pbyteSrcBase + j * iSrcImageWidthStep;
                                byte[] pbyteSrc = new byte[pbyteSrcBase.Length + j * iSrcImageWidthStep];
                                //unsigned char* pbyteDest = pbyteDestBase + j * 3;
                                byte[] pbyteDest = new byte[pbyteDestBase.Length + j * 3];
                                pbyteDest[0] = pbyteSrc[0];
                                pbyteDest[1] = pbyteSrc[1];
                                pbyteDest[2] = pbyteSrc[2];
                            }
                        }
                        if (pbyteBitsSrc != null)
                        {
                            //delete[] pbyteBitsSrc;
                            pbyteBitsSrc = null;
                        }
                        SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, pbyteBitsDest);
                        if (pbyteBitsDest != null)
                        {
                            //delete[] pbyteBitsDest;
                            pbyteBitsDest = null;
                        }
                        
        }

        public void RotateAntiClockwiseBackgroundBitmap(Graphics hDCRef)
        {
            int iSrcImageWidth = m_iBackgroundWidth;
            int iSrcImageHeight = m_iBackgroundHeight;
            int iSrcImageWidthStep = (iSrcImageWidth * 3 + 3) / 4 * 4;
            int iSrcImageAllocSize = iSrcImageWidthStep * iSrcImageHeight;
            if (iSrcImageAllocSize <= 0)
            {
                return;
            }
            int iDestImageWidth = m_iBackgroundHeight;
            int iDestImageHeight = m_iBackgroundWidth;
            int iDestImageWidthStep = (iDestImageWidth * 3 + 3) / 4 * 4;
            int iDestImageAllocSize = iDestImageWidthStep * iDestImageHeight;
            if (iDestImageAllocSize <= 0)
            {
                return;
            }
            //unsigned char* pbyteBitsSrc = new unsigned char[iSrcImageAllocSize];
            byte[] pbyteBitsSrc = new byte[iSrcImageAllocSize];
            //unsigned char* pbyteBitsDest = new unsigned char[iDestImageAllocSize];
            byte[] pbyteBitsDest = new byte[iDestImageAllocSize];
            if ((pbyteBitsSrc == null) || (pbyteBitsDest == null))
            {
                if (pbyteBitsSrc != null)
                {
                    //delete[] pbyteBitsSrc;
                    pbyteBitsSrc = null;
                }
                if (pbyteBitsDest != null)
                {
                    //delete[] pbyteBitsDest;
                    pbyteBitsDest = null;
                }
            }
            GetBackgroundBitmap(hDCRef, pbyteBitsSrc);
            int i = 0;
            int j = 0;
            for (i = 0; i < iDestImageHeight; i++)
            {
                //const unsigned char* pbyteSrcBase = pbyteBitsSrc + 3 * (iSrcImageWidth - 1 - i);
                byte[] pbyteSrcBase = new byte[pbyteBitsSrc.Length + 3 * (iSrcImageWidth - 1 - i)];
                //unsigned char* pbyteDestBase = pbyteBitsDest + (iDestImageHeight - 1 - i) * iDestImageWidthStep;
                byte[] pbyteDestBase = new byte[pbyteBitsDest.Length + (iDestImageHeight - 1 - i) * iDestImageWidthStep];
                for (j = 0; j < iDestImageWidth; j++)
                {
                    //const unsigned char* pbyteSrc = pbyteSrcBase + (iSrcImageHeight - 1 - j) * iSrcImageWidthStep;
                    byte[] pbyteSrc = new byte[pbyteSrcBase.Length + (iSrcImageHeight - 1 - j) * iSrcImageWidthStep];
                    //unsigned char* pbyteDest = pbyteDestBase + j * 3;
                    byte[] pbyteDest = new byte[pbyteDestBase.Length + j * 3];
                    pbyteDest[0] = pbyteSrc[0];
                    pbyteDest[1] = pbyteSrc[1];
                    pbyteDest[2] = pbyteSrc[2];
                }
            }
            if (pbyteBitsSrc != null)
            {
                //delete[] pbyteBitsSrc;
                pbyteBitsSrc = null;
            }
            SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, pbyteBitsDest);
            if (pbyteBitsDest != null)
            {
                //delete[] pbyteBitsDest;
                pbyteBitsDest = null;
            }
        }

        public void SetDragAcceptFilesFlag(bool bAccept)
        {
            m_bDragAcceptFilesFlag = bAccept;
        }

        public string GetDragFileName()
        {
            return m_strDragFileName;
        }

        /*        public void OnDropFiles(HDROP hDropInfo)
        {
            if (DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0) > 0)
            {
                int iFileNameSize = DragQueryFile(hDropInfo, 0, NULL, 0);
                if (iFileNameSize > 0)
                {
                    char* pchFileName = new char[iFileNameSize + 1];
                    if (pchFileName != NULL)
                    {
                        DragQueryFile(hDropInfo, 0, pchFileName, iFileNameSize + 1);
                        m_strDragFileName = pchFileName;
                        delete[] pchFileName;
                        pchFileName = NULL;
                        if (GetParent() != NULL)
                        {
                            GetParent()->SendMessage(WM_COMMAND, GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                        }
                    }
                }
            }

            ::DragFinish(hDropInfo);
            hDropInfo = NULL;
        }*/



    }
}
