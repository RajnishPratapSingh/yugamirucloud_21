﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomControls
{
    public partial class PC : UserControl
    {
        Color a;
        public string strRemarks { get; set; }
        public string strRegUserId { get; set; }
        public string strStallId { get; set; }
        public string strLanguage { get; set; }
        public string strComputerName { get; set; }
        public string strInstallationName { get; set; }
        public string strDateOfInstall { get; set; }
        public string strInstalledBy { get; set; }
        public string strComments { get; set; }
        public bool blSync { get; set; }
        public PC()
        {
            a = this.BackColor;
            InitializeComponent();
        }
        public void SetCommentTip(string tipText)
        {
            toolTip1.SetToolTip(pictureBox1, tipText);
        }
        private void chkSelected_CheckedChanged(object sender, EventArgs e)
        {
            if(chkSelected.Checked)
            {
                this.BackColor = Color.PaleGreen;
            }
            else
            {
                this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            }
        }

        private void txtInsName_TextChanged(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtInsName, txtInsName.Text);
        }

        private void txtPCName_TextChanged(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtPCName, txtPCName.Text);
        }

        private void txtInstallDate_TextChanged(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtInstallDate, txtInstallDate.Text);
        }

        private void PC_EnabledChanged(object sender, EventArgs e)
        {
            if(this.Enabled==false)
            {
                this.BackColor = System.Drawing.SystemColors.Desktop;
            }
        }
    }
}
